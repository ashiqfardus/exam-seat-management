<?php
/**
 * Created by PhpStorm.
 * User: Ashiq Fardus
 * Date: 4/21/2018
 * Time: 6:01 PM
 */
include 'includes/connection.php';
if (!isset($_SESSION))
{
    session_start();
}
if(!isset($_SESSION['email']))
{
    header("location: u_log.php");
}
else
{
    $email=$_SESSION['email'];
    $query="SELECT * FROM student_information WHERE email= '$email' OR student_id='$email'";
    $run=mysqli_query($connection,$query);
    while ($data=mysqli_fetch_array($run))
    {
        $id=$data['id'];
        $image=$data['image'];
        $student_id=$data['student_id'];
        $name=$data['name'];
        $phone=$data['phone'];
        $dob=$data['dob'];
        $gender=$data['gender'];
        ?>
        <!DOCTYPE html>
        <html >
        <head>
            <meta charset="UTF-8">
            <title>Exam Seat Handling</title>
            <link rel='stylesheet prefetch' href='css/bootstrap.css'>
            <link rel="stylesheet" href="css/style.css">
            <link rel="stylesheet" href="css/custom.css">
            <link rel="stylesheet" href="css/font-awesome.min.css">
        </head>

        <body>
        <!-- Navbar top -->
        <?php include('includes/student_nav.php'); ?>
        <!-- Navbar end here-->



        <!--Page Body-->
        <div class="container row_margin_h row_margin_bottom">
            <div class="col-md-3">
            </div>
            <?php
            function fill_semester($connection)
            {
                $output='';
                $sql="SELECT * FROM semester ORDER BY semester_id";
                $result= mysqli_query($connection,$sql);
                while ($row=mysqli_fetch_array($result))
                {
                    $output.='<option value="'.$row['semester_no'].'">'.$row['semester_no'].'</option>';
                }
                return $output;
            }
            function fill_course($connection)
            {
                $output="";
                $student_id=$_GET['id'];
                $sql="SELECT * FROM course WHERE student_id='$student_id'";
                $result=mysqli_query($connection,$sql);
                while ($row=mysqli_fetch_array($result))
                {
                    $output.='<tr>';
                    $output.='<td>'.$row['course_code'].'</td>';
                    $output.='<td>'.$row['section'].'</td>';
                    $output.='<td>'.$row['semester'].'</td>';
                    $output.='</tr>';
                }
                return $output;
            }
            function fill_course_semester($connection)
            {
                $output="";
                $student_id=$_GET['id'];
                if (isset($_POST['submit']))
                {
                    $semester=$_POST['semester'];
                    $sql="SELECT * FROM course WHERE student_id='$student_id' AND semester='$semester'";
                    $result=mysqli_query($connection,$sql);
                    while ($row=mysqli_fetch_array($result))
                    {
                        $output.='<tr>';
                        $output.='<td>'.$row['course_code'].'</td>';
                        $output.='<td>'.$row['section'].'</td>';
                        $output.='<td>'.$row['semester'].'</td>';
                        $output.='</tr>';
                    }
                    return $output;
                }
            }
            ?>
            <div class="col-md-12 column_color">
                <form action="course.php?id=<?php echo $student_id?>" method="POST">
                    <div class="form-group">
                        <label for="sel1">Select Semester:</label>
                        <select class="form-control" name="semester" onchange="showUser(this.value)">
                            <option value="">Select Semester</option>
                            <?php echo fill_semester($connection)?>
                        </select>
                    </div>
                    <button class="form-group button_alignment btn-default btn" name="submit" type="submit">Submit</button>
                </form>
                    <div class="form-group">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Course Code</th>
                                <th>Section</th>
                                <th>Semester</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                if (isset($_POST['submit']))
                                {
                                    echo fill_course_semester($connection);
                                }
                                else
                                {
                                    echo fill_course($connection);
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
            </div>
            <div class="col-md-3">
            </div>
        </div>
        <!-- Body End-->


        <!--NAavbar bottom-->

        <?php include('includes/footer.php'); ?>

        <!--JavaScript here-->
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script  src="js/index.js"></script>


        </body>
        </html>
    <?php } }?>

