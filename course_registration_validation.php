<?php
/**
 * Created by PhpStorm.
 * User: Ashiq Fardus
 * Date: 5/15/2018
 * Time: 8:25 PM
 */
    require 'includes/connection.php';
    function course_reg()
    {
        global $connection;
        $result=array(
            'result'=>1,
            'message'=>'undefined error'
        );
        if (isset($_POST['submit']))
        {
            $student_id=$_GET['id'];
            $semester=$_POST['semester'];
            $course=$_POST['course'];
            $section=$_POST['section'];
            $course=strtolower($course);
            $section=strtolower($section);

            //Check registration
            $sql="SELECT * FROM course WHERE student_id='$student_id' AND course_code='$course'";
            $run=mysqli_query($connection,$sql);
            if (mysqli_num_rows($run)>0)
            {
                $result['message']='Course already registered';
                return $result;
            }

            //Empty field check
            if (empty($semester))
            {
                $result['message']='Select Semester';
                return $result;
            }
            if (empty($course))
            {
                $result['message']='Course code empty.';
                return $result;
            }
            if (empty($section))
            {
                $result['message']='Section is empty.';
                return $result;
            }

            //Inserting query
            $query="INSERT INTO course(course_code,student_id,semester,section) VALUES('$course','$student_id','$semester','$section')";
            if (mysqli_query($connection,$query))
            {
                $result['result']=0;
                $result['message']="Course registration completed";
                return $result;
            }
        }
    }