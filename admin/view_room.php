<?php require '../includes/connection.php';
if (!isset($_SESSION))
{
    session_start();
}
if(!isset($_SESSION['email']))
{
    header("location: admin_login.php");
}
else
{
    $email=$_SESSION['email'];
    $query="SELECT * FROM admin WHERE email= '$email' OR username='$email'";
    $run=mysqli_query($connection,$query);
    while ($data=mysqli_fetch_array($run))
    {
        $id=$data['a_id'];
        $username=$data['username'];
        $email=$data['email'];
        ?>
        <!DOCTYPE html>
        <html >
        <head>
            <meta charset="UTF-8">
            <title>Exam Seat Handling</title>
            <link rel='stylesheet prefetch' href='../css/bootstrap.css'>
            <link rel="stylesheet" href="../css/style.css">
            <link rel="stylesheet" href="../css/custom.css">
            <link rel="stylesheet" href="../css/font-awesome.min.css">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

        </head>

        <body>
        <!-- Navbar top -->
        <?php include('../includes/admin_home_nav.php'); ?>
        <!-- Navbar end here-->

        <div class="row_margin_h">
            <h2 class="header_text"> Welcome to Exam Seat Management System</h2>
        </div>
        <div class="col-md-4">

        </div>

        <div class="col-md-4 column_color row_margin_bottom">
            <h3 class="header_text">Room List</h3>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Room Number</th>
                        <th>Capacity</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $query=mysqli_query($connection,"SELECT * FROM room");
                            while($row=mysqli_fetch_array($query))
                            {
                                $id=$row['id'];
                                $room_no=$row['room_no'];
                                $capacity=$row['capacity'];
                    ?>

                    <tr>
                        <td><?php echo $id ?></td>
                        <td><?php echo $room_no ?></td>
                        <td><?php echo $capacity ?></td>
                        <td><a href="update_room.php?id=<?php echo $id?>" class="btn btn-info btn-group-sm"><i class="fas fa-pencil-alt"></i></a></td>
                        <td><a href="delete_room.php?id=<?php echo $id?>" class="btn btn-danger btn-group-sm"><i class="fas fa-trash-alt"></i></a></td>
                    </tr>
                                <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-4">

        </div>



        <!--NAavbar bottom-->

        <?php include('../includes/footer.php'); ?>

        <!--JavaScript here-->
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

        <script  src="../js/index.js"></script>

        </body>
        </html>
    <?php }} ?>