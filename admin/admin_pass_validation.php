<?php
/**
 * Created by PhpStorm.
 * User: Ashiq Fardus
 * Date: 3/11/2018
 * Time: 7:37 PM
 */

include '../includes/connection.php';

function spass()
{
    global $connection;
    if (isset($_POST['submit'])) {
        $id = $_GET['id'];
        $old_password = $_POST['old_password'];

        $password = $_POST['password'];
        $password2 = $_POST['password2'];

        //Check empty field

        if (empty($password) || empty($password2) || empty($old_password)) {
            $result['result'] = 1;
            $result['message'] = 'Any Of the field is empty';
            return $result;
        }

        //Password matching
        if ($old_password==$password)
        {
            $result['result']=1;
            $result['message']="You can't use your current password as new password.";
            return $result;
        }

        if ($password != $password2) {
            $result['result'] = 1;
            $result['message'] = "Password didn't match";
            return $result;
        }
        if (strlen($password)>20 || strlen($password)<6)
        {
            $result['result'] = 1;
            $result['message']='Password length must be between 6-20 character';
            return $result;
        }

        //Old pass check
        $old_password = md5($old_password);
        $pass_query = mysqli_query($connection, "SELECT * FROM admin WHERE a_id='$id' AND password='$old_password'");

        $pass_no = mysqli_num_rows($pass_query);
        if ($pass_no > 0) {
            $password = md5($password);
            $query = "UPDATE admin SET password='$password' WHERE a_id='$id'";
            if (mysqli_query($connection, $query)) {
                $result['result'] = 0;
                $result['message'] = 'Password successfully changed.';
                return $result;
            }
        } else {
            $result['result'] = 1;
            $result['message'] = "Old password didn't match";
            return $result;
        }
    }
}
?>