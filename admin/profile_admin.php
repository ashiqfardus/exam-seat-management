<?php require '../includes/connection.php';
if (!isset($_SESSION))
{
    session_start();
}
if(!isset($_SESSION['email']))
{
    header("location: admin_login.php");
}
else
{
$email=$_SESSION['email'];
$query="SELECT * FROM admin WHERE email= '$email' OR username='$email'";
$run=mysqli_query($connection,$query);
while ($data=mysqli_fetch_array($run))
{
$id=$data['a_id'];
$username=$data['username'];
$email=$data['email'];
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Exam Seat Handling</title>
    <link rel='stylesheet prefetch' href='../css/bootstrap.css'>
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/custom.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">

  
</head>

<body>
	<!-- Navbar -->
    
	<?php include('../includes/admin_home_nav.php'); ?>
	
	<!-- Login and Registration Form-->
        <div class="container row_margin_bottom">
            <div class="container row_margin_h">
                <div class="row row_margin_bottom">
                    <div class="col-md-3">
                        
                    </div>
                    <div class="col-md-6 column_color">
                        <p class="p_text">Username: <?php echo $username?> </p>
                        <p class="p_text">Email: <?php echo $email?> </p>
                    </div>
                    <div class="col-md-3">
                        
                    </div>
                </div>
            </div>
        </div>
		<!-- Login and registration form end here-->


        <!-- Navbar bottom-->
		<?php include('../includes/footer.php'); ?>
		<!--JavaScript here-->
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

    <script  src="../js/index.js"></script>

</body>
</html>
<?php }} ?>