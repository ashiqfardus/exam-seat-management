<?php
/**
 * Created by PhpStorm.
 * User: Ashiq Fardus
 * Date: 7/11/2018
 * Time: 6:50 PM
 */
require '../includes/connection.php';
if (!isset($_SESSION))
{
    session_start();
}
if(!isset($_SESSION['email']))
{
    header("location: admin_login.php");
}
else
{
    if (isset($_GET['id']))
    {
        $id=$_GET['id'];
        $sql="DELETE from room_details where r_id='$id'";
        if (mysqli_query($connection,$sql))
        {
            ?>
            <script>
                window.location="seatplan.php";
            </script>
            <?php
        }
    }
}