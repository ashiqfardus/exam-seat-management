<?php
/**
 * Created by PhpStorm.
 * User: Ashiq Fardus
 * Date: 5/29/2018
 * Time: 11:29 PM
 */
require 'includes/connection.php';
if (!isset($_SESSION))
{
    session_start();
}
if(!isset($_SESSION['email']))
{
    header("location: u_log.php");
}
else
{
    $email=$_SESSION['email'];
    $query="SELECT * FROM student_information WHERE email= '$email' OR student_id='$email'";
    $run=mysqli_query($connection,$query);
    while ($data=mysqli_fetch_array($run))
    {
        $id=$data['id'];
        $student_id=$data['student_id'];
        $name=$data['name'];
        $phone=$data['phone'];
        $dob=$data['dob'];
        $gender=$data['gender'];
        ?>
        <!DOCTYPE html>
        <html >
        <head>
            <meta charset="UTF-8">
            <title>Exam Seat Handling</title>
            <link rel='stylesheet prefetch' href='css/bootstrap.css'>
            <link rel="stylesheet" href="css/style.css">
            <link rel="stylesheet" href="css/custom.css">
            <link rel="stylesheet" href="css/font-awesome.min.css">

        </head>

        <body>
        <!-- Navbar top -->
        <?php include('includes/student_nav.php'); ?>
        <!-- Navbar end here-->



        <!--Page Body-->
        <div class="container row_margin_h">
            <h1 class="button_alignment" style="margin-bottom: 20px;"> Scan Your RFID Card </h1>
            <div class="row row_margin_bottom">
                <form class="form form-vertical" action="add_rfid.php?id=<?php echo $student_id ?>" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="name">RFID Tag<span class="kv-reqd"></span></label>
                                        <input type="text" class="form-control" name="rfid_tag">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="text-left">
                                    <button type="submit" name="submit" class="btn btn-default">Submit</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- Body End-->

        <?php
        require_once 'add_rfid_validation.php';
        $result=rfid();

        //        Showing messages

        if ($result)
        {
            if ($result['result']==0)
            {
                ?>
                <div class="col-md-4">

                </div>
                <div class="col-md-4">
                    <div class="alert alert-success" role="alert">
                        <strong>Success!</strong> <?php echo $result['message']; ?>
                    </div>
                </div>
                <script>
                    window.location.href='student_homepage.php';
                </script>
            <?php
            }
            else
            {
            ?>
                <div class="col-md-4">

                </div>
                <div class="col-md-4">
                    <div class="alert alert-danger" role="alert">
                        <strong>Error!</strong> <?php echo $result['message']; ?>
                    </div>
                </div>
                <?php
            }
        }
        ?>
        <!--NAavbar bottom-->

        <?php include('includes/footer.php'); ?>

        <!--JavaScript here-->
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

        <script  src="js/index.js"></script>

        </body>
        </html>
    <?php } } ?>