<?php
require '../includes/connection.php';
if (!isset($_SESSION))
{
session_start();
}
if(!isset($_SESSION['email']))
{
header("location: admin_login.php");
}
else
{
$email=$_SESSION['email'];
$query="SELECT * FROM admin WHERE email= '$email' OR username='$email'";
$run=mysqli_query($connection,$query);
while ($data=mysqli_fetch_array($run))
{
$id=$data['a_id'];
$username=$data['username'];
$email=$data['email'];
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Exam Seat Handling</title>
    <link rel='stylesheet prefetch' href='../css/bootstrap.css'>
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/custom.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">

  
</head>

<body>
	<!-- Navbar -->
    
	<?php include('../includes/admin_home_nav.php'); ?>
	
	<!-- Body Start-->
        <div class="container row_margin_bottom">
            <div class="row row_margin_home">
                <h2 class="header_text"> Welcome to Exam Seat Management System</h2>
            </div>
                <div class="row row_margin">
                <!-- Details part-->
                <div class="col-md-3">
                </div>
                <div class="col-md-8 column_color">
                    <div class="row">
                        <h3 class="header_text">Details</h3>
                    </div>
                    <div class="row row_margin">
                        <p class="p_text">Username: <?php echo $username?> </p>
                        <p class="p_text">Email: <?php echo $email?> </p>
                    </div>
                </div>
                <div class="col-md-3">
                </div>
           </div>
           <div class="row row_margin row_margin_bottom">
                <div class="col-md-12 column_color">
                    <h3 class="button_alignment">Instructions </h3>
                    <p class="p_text"> 1. You Can change you username and email from menu.</p>
                    <p class="p_text"> 2. If you enter any wrong data then authority will not be responsible. </p>
                    <p class="p_text"> 3. You Can change you password from menu. </p>
                </div>
           </div>
        </div>
		<!-- Body End-->


        <!-- Navbar bottom-->
		<?php include('../includes/footer.php'); ?>
		<!--JavaScript here-->
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

    <script  src="../js/index.js"></script>

</body>
</html>
<?php }} ?>