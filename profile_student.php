<?php
include 'includes/connection.php';
if (!isset($_SESSION))
{
    session_start();
}
if(!isset($_SESSION['email']))
{
    header("location: u_log.php");
}
else
{
    $email=$_SESSION['email'];
    $query="SELECT * FROM student_information WHERE email= '$email' OR student_id='$email'";
    $run=mysqli_query($connection,$query);
    while ($data=mysqli_fetch_array($run))
    {
        $id=$data['id'];
        $image=$data['image'];
        $student_id=$data['student_id'];
        $name=$data['name'];
        $phone=$data['phone'];
        $dob=$data['dob'];
        $gender=$data['gender'];
        $semester=$data['semester'];
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Exam Seat Handling</title>
    <link rel='stylesheet prefetch' href='css/bootstrap.css'>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
  
</head>

<body>
	<!-- Navbar top -->
		<?php include('includes/student_nav.php'); ?>
	<!-- Navbar end here-->



	<!--Page Body-->
	    <div class="container row_margin_h">
	    	<div class="row row_margin_bottom">
	    		<div class="col-md-12 column_color button_alignment">
	    			<img class="img-circle" style="height: 200px; width: 200px;" src="image/<?php echo $image ?>">

	    			<p class="p_text">Name: <?php echo $name ?> </p>
	       			<p class="p_text">Id: <?php echo $student_id ?> </p>
	       			<p class="p_text">Email: <?php echo $email ?> </p>
	       			<p class="p_text">Semester: <?php echo $semester ?> </p>
	       			<p class="p_text">Phone Number: <?php echo $phone ?> </p>
	       			<p class="p_text">Gender: <?php echo $gender ?> </p>
	       			<p class="p_text">Gender: <?php echo $dob ?> </p>
	    		</div>
	    	</div>
	    </div>
    <!-- Body End-->


    <!--NAavbar bottom-->
    
	<?php include('includes/footer.php'); ?>
		
		<!--JavaScript here-->
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

    <script  src="js/index.js"></script>

</body>
</html>
<?php } }?>