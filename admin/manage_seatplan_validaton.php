<?php
/**
 * Created by PhpStorm.
 * User: Ashiq Fardus
 * Date: 6/24/2018
 * Time: 9:30 PM
 */
require '../includes/connection.php';

function seatplan()
{
    global $connection;
//    Using result for error messages
    $result = array(
        'result' => '1',
        'message' => 'Undefined Error'
    );

    if (isset($_POST['submit']))
    {
        $semester=$_POST['semester'];
        $date=$_POST['date'];
        $room_no=$_POST['room_no'];
        $time=$_POST['time'];
        $initial1=$_POST['initial1'];
        $initial2=$_POST['initial2'];
        $course_code=$_POST['course_code'];
        $section=$_POST['section'];
        $column_no=$_POST['column_no'];
        $total_seat=$_POST['total_seat'];

        //Empty field check
        if (empty($semester))
        {
            $result['message']="Select semester";
            return $result;
        }

        if (empty($date))
        {
            $result['message']="Select date";
            return $result;
        }

        if (empty($room_no))
        {
            $result['message']="Select Room";
            return $result;
        }
        if (empty($time))
        {
            $result['message']="Select Time";
            return $result;
        }

        if (empty($initial1))
        {
            $result['message']="Select Teacher Initial";
            return $result;
        }
        if (empty($course_code))
        {
            $result['message']="Select Course Code";
            return $result;
        }
        if (empty($section))
        {
            $result['message']="Select Section";
            return $result;
        }
        if (empty($column_no))
        {
            $result['message']="Select Column No";
            return $result;
        }
        if (empty($total_seat))
        {
            $result['message']="Select Total Seat";
            return $result;
        }

        //Initial matching

        if ($initial1==$initial2)
        {
            $result['message']="Both initials are same";
            return $result;
        }

        //Teacher assigning check
        $sql=mysqli_query($connection,"SELECT * FROM room_details where 
                                          (teacher1='$initial1' or teacher2='$initial1') and time='$time' and date='$date'");
        $res=mysqli_num_rows($sql);
        if ($res>0)
        {
            $result['message']="'$initial1' is already assigned";
            return $result;
        }

        if (!empty($initial2)) {
            $sql = mysqli_query($connection, "SELECT * FROM room_details where 
                                          (teacher1='$initial2' or teacher2='$initial2') and time='$time' and date='$date'");
            $res = mysqli_num_rows($sql);
            if ($res > 0) {
                $result['message'] = "'$initial2' is already assigned";
                return $result;
            }
        }

        //Column check
        $sql=mysqli_query($connection,"SELECT * FROM room_details where  
                                            room_no='$room_no' and time='$time' 
                                            and date='$date' and column_no like '%$column_no%'");

        $run=mysqli_num_rows($sql);
        if ($run>0)
        {
            $result['message']="Columns are already assigned";
            return $result;
        }

        //Seat count
        $sum_sql=mysqli_query($connection,"SELECT SUM(total_seat) AS total_seat from room_details where room_no='$room_no' and date='$date' and time='$time'");
        $row=mysqli_fetch_assoc($sum_sql);
        $seat=$row['total_seat'];
        $f_seat=$seat+$total_seat;
        $r_sql=mysqli_query($connection,"SELECT capacity from room where room_no='$room_no'");
        $res_seat=mysqli_fetch_array($r_sql);
        $capacity=$res_seat['capacity'];
        $fseat=$capacity-$seat;
        if ($f_seat>$capacity)
        {
            $result['message']="$fseat seats are available";
            return $result;
        }




        //Insert query
        $query="INSERT INTO room_details 
                (room_no,time,teacher1,teacher2,course_code,
                section,column_no, total_seat,semester,date,remaining_seat) VALUES 
                ('$room_no','$time','$initial1','$initial2','$course_code',
                '$section','$column_no','$total_seat','$semester','$date','$total_seat')";
        if (mysqli_query($connection,$query))
        {
            $result['result']=0;
            $result['message']='Successfully completed ';
            return $result;
        }
    }
}
