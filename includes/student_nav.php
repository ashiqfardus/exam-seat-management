<?php require 'connection.php';
if (!isset($_SESSION))
{
    session_start();
}
if(!isset($_SESSION['email']))
{
    header("location: u_log.php");
}
else
{
    $email=$_SESSION['email'];
    $query="SELECT * FROM student_information WHERE email= '$email' OR student_id='$email'";
    $run=mysqli_query($connection,$query);
    while ($data=mysqli_fetch_array($run))
    {
        $id=$data['id'];
        $student_id=$data['student_id'];
        $name=$data['name'];
        $phone=$data['phone'];
        $dob=$data['dob'];
        $gender=$data['gender'];
        $rfid=$data['rfid_tag'];
    ?>
<div class="container-fluid">
		<nav class="navbar navbar-default nav_color navbar-fixed-top">
		  <div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="student_homepage.php">ESM</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <ul class="nav navbar-nav">
				<li><a href="seatplan_student.php?id=<?php echo $student_id?>">Seatplan</a></li>
				<li><a href="course.php?id=<?php echo $student_id?>">Course</a></li>
				<li><a href="course_registration.php?id=<?php echo $student_id?>">Registration</a></li>
				<li><a href="semester.php?id=<?php echo $student_id?>">Semester</a></li>
                  <?php
                  if (empty($rfid)) {
                      ?>
                      <li><a href="add_rfid.php?id=<?php echo $student_id ?>">Add RFID</a></li>
                      <?php
                  }
                  ?>

			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li><a href="profile_student.php?id=<?php echo $id ?>"><?php echo $name?></a></li>
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu <span class="caret"></span></a>
				  <ul class="dropdown-menu">
				  	<li><a href="profile_student.php?id=<?php echo $id ?>"><i class="fa fa-user" aria-hidden="true"></i> profile</a></li>
					<li><a href="edit_student_profile.php?id=<?php echo $id ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Profile</a></li>
					<li><a href="seatplan_student.php?id=<?php echo $student_id?>"><i class="fa fa-list-ol" aria-hidden="true"></i> Seat Plan</a></li>
					<li><a href="course_registration.php?id=<?php echo $id ?>"><i class="fa fa-registered" aria-hidden="true"></i> Course Registration</a></li>
					<li><a href="Update_registration.php?id=<?php echo $id ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Update Registration</a></li>
					<li><a href="student_change_pass.php?id=<?php echo $id ?>"><i class="fa fa-unlock-alt" aria-hidden="true"></i> Change Password</a></li>
					<li role="separator" class="divider"></li>
					
					<li><a href="logout.php"><i class="fa fa-user" aria-hidden="true"></i> Logout</a></li>
				  </ul>
				</li>
			  </ul>
			</div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</div>
	<script src="https://use.fontawesome.com/6e655bd209.js"></script>

<?php } } ?>