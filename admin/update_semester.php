<?php
/**
 * Created by PhpStorm.
 * User: Ashiq Fardus
 * Date: 5/31/2018
 * Time: 12:59 AM
 */
require '../includes/connection.php';
if (!isset($_SESSION))
{
    session_start();
}
if(!isset($_SESSION['email']))
{
    header("location: admin_login.php");
}
else
{
    $email=$_SESSION['email'];
    $query="SELECT * FROM admin WHERE email= '$email' OR username='$email'";
    $run=mysqli_query($connection,$query);
    while ($data=mysqli_fetch_array($run))
    {
        $id=$data['a_id'];
        $username=$data['username'];
        $email=$data['email'];
        ?>
        <!DOCTYPE html>
        <html >
        <head>
            <meta charset="UTF-8">
            <title>Exam Seat Handling</title>
            <link rel='stylesheet prefetch' href='../css/bootstrap.css'>
            <link rel="stylesheet" href="../css/style.css">
            <link rel="stylesheet" href="../css/custom.css">
            <link rel="stylesheet" href="../css/font-awesome.min.css">

        </head>

        <body>
        <!-- Navbar top -->
        <?php include('../includes/admin_home_nav.php'); ?>
        <!-- Navbar end here-->


        <?php
        if (isset($_GET['id']))
        {
            $id=$_GET['id'];
            $sql="SELECT * FROM semester where semester_id='$id'";
            $run=mysqli_query($connection,$sql);
            $result=mysqli_fetch_array($run);
            $id = $result['semester_id'];
            $semester = $result['semester_no'];


            ?>
            <!--Page Body-->
            <div class="container row_margin_h">
                <h1 class="button_alignment" style="margin-bottom: 20px;"><B>Update Semester </B></h1>
                <div class="row row_margin_bottom">
                    <form class="form form-vertical" action="update_semester.php?id=<?php echo $id ?>" method="post">
                        <div class="row">
                            <div class="col-sm-2">
                            </div>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="name">Enter Semester<span class="kv-reqd"></span></label>
                                            <input type="text" class="form-control" name="semester" value="<?php echo $semester?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="text-center">
                                        <button type="submit" name="submit" class="btn btn-default">Submit</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <?php
        }
        ?>
        <!-- Body End-->

        <!--Edit action -->
        <?php
        require_once 'add_semester_validation.php';
        $result=update_semester();

        //        Showing messages

        if ($result)
        {
            if ($result['result']==0)
            {
                ?>
                <div class="col-md-4">
                </div>

                <div class="col-md-4 row_margin">
                    <div class="alert alert-success" role="alert">
                        <?php echo $result['message']; ?>
                    </div>
                </div>
                <script>
                    window.setTimeout(function(){

                        // Move to a new location or you can do something else
                        window.location.href = "view_semester.php";

                    }, 1000);
                </script>
            <?php
            }
            else
            {
            ?>

                <div class="col-md-4">
                </div>

                <div class="col-md-4 row_margin">
                    <div class="alert alert-danger" role="alert">
                        <strong>Error!</strong> <?php echo $result['message']; ?>
                    </div>
                </div>

                <?php
            }
        }
        ?>


        <!--NAavbar bottom-->

        <?php include('../includes/footer.php'); ?>

        <!--JavaScript here-->
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

        <script  src="../js/index.js"></script>

        </body>
        </html>
    <?php }} ?>