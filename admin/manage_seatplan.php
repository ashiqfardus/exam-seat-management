<?php require '../includes/connection.php';
if (!isset($_SESSION))
{
    session_start();
}
if(!isset($_SESSION['email']))
{
    header("location: admin_login.php");
}
else
{
$email=$_SESSION['email'];
$query="SELECT * FROM admin WHERE email= '$email' OR username='$email'";
$run=mysqli_query($connection,$query);
while ($data=mysqli_fetch_array($run))
{
$id=$data['a_id'];
$username=$data['username'];
$email=$data['email'];
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Exam Seat Handling</title>
    <link rel='stylesheet prefetch' href='../css/bootstrap.css'>
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/custom.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">

  
</head>

<body>
	<!-- Navbar -->
    
	<?php include('../includes/admin_home_nav.php'); ?>
	
    <!--Body start-->

        <div class="container row_margin_bottom">
            <h1 class="header_text row_margin_home"> Assign Seatplan</h1>
            <div class="col-md-3">
            </div>
            <div class="col-md-6 column_color">
                <form method="post" action="manage_seatplan.php">
                    <div class="form-group">
                        <label for="sel1">Select Semester:</label>
                        <select class="form-control" id="sel1" name="semester">
                            <option value="">Select Semester</option>
                            <?php
                            $sql="SELECT * FROM semester ORDER BY semester_no";
                            $run=mysqli_query($connection,$sql);
                            while ($result=mysqli_fetch_array($run)) {
                                $semester_id = $result['semester_id'];
                                $semester_no = $result['semester_no'];
                                ?>
                                <option value="<?php echo $semester_no ?>"><?php echo $semester_no?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="sel1">Date:</label>
                        <input class="form-control" type="date" name="date">
                    </div>

                  <div class="form-group row">

                      <div class="col-xs-6">
                        <label for="ex3">Room No</label>
                          <select class="form-control" id="sel1" name="room_no">
                              <option value="">Select Room</option>
                              <?php
                              $sql="SELECT * FROM room ORDER BY room_no";
                              $run=mysqli_query($connection,$sql);
                              while ($result=mysqli_fetch_array($run)) {
                                  $id = $result['id'];
                                  $room_no = $result['room_no'];
                                  ?>
                                  <option value="<?php echo $room_no ?>"><?php echo $room_no?></option>
                                  <?php
                              }
                              ?>
                          </select>
                      </div>
                      <div class="col-xs-6">
                          <label for="ex3">Time</label>
                          <select class="form-control" id="sel1" name="time">
                              <option value="">Select Time</option>
                              <?php
                              $sql="SELECT * FROM time ORDER BY time_list";
                              $run=mysqli_query($connection,$sql);
                              while ($result=mysqli_fetch_array($run)) {
                                  $id = $result['id'];
                                  $time = $result['time_list'];
                                  ?>
                                  <option value="<?php echo $time ?>"><?php echo $time?></option>
                                  <?php
                              }
                              ?>
                          </select>
                      </div>

                  </div>
                  <div class="form-group row">

                      <div class="col-xs-6">
                          <label for="ex3">Teacher initial 1</label>
                          <select class="form-control" id="sel1" name="initial1">
                              <option value="">Select Teacher Initial</option>
                              <?php
                              $sql="SELECT initial FROM teacher ORDER BY initial";
                              $run=mysqli_query($connection,$sql);
                              while ($result=mysqli_fetch_array($run)) {
                                  $id = $result['t_id'];
                                  $initial = $result['initial'];
                                  ?>
                                  <option value="<?php echo $initial ?>"><?php echo $initial?></option>
                                  <?php
                              }
                              ?>
                          </select>
                      </div>
                      <div class="col-xs-6">
                          <label for="ex3">Teacher initial 2</label>
                          <select class="form-control" id="sel1" name="initial2">
                              <option value="">Select Teacher Initial</option>
                              <?php
                              $sql="SELECT t_id,initial FROM teacher ORDER BY initial";
                              $run=mysqli_query($connection,$sql);
                              while ($result=mysqli_fetch_array($run)) {
                                  $id = $result['t_id'];
                                  $initial = $result['initial'];
                                  ?>
                                  <option value="<?php echo $initial ?>"><?php echo $initial?></option>
                                  <?php
                              }
                              ?>
                          </select>
                      </div>

                  </div>
                  <div class="form-group row">

                      <div class="col-xs-6">
                          <label for="ex3">Course Code</label>
                          <select class="form-control" id="sel1" name="course_code">
                              <option value="">Select Course Code</option>
                              <?php
                              $sql="SELECT distinct course_code  FROM course";
                              $run=mysqli_query($connection,$sql);
                              while ($result=mysqli_fetch_array($run)) {
                                  $id = $result['c_id'];
                                  $course_code = $result['course_code'];
                                  ?>
                                  <option value="<?php echo $course_code ?>"><?php echo $course_code?></option>
                                  <?php
                              }
                              ?>
                          </select>
                      </div>
                      <div class="col-xs-6">
                          <label for="ex3">Section</label>
                          <select class="form-control" id="sel1" name="section">
                              <option value="">Select Section</option>
                              <?php
                              $sql="SELECT distinct section FROM course ORDER BY section";
                              $run=mysqli_query($connection,$sql);
                              while ($result=mysqli_fetch_array($run)) {
                                  $id = $result['c_id'];
                                  $section = $result['section'];
                                  ?>
                                  <option value="<?php echo $section ?>"><?php echo $section?></option>
                                  <?php
                              }
                              ?>
                          </select>
                      </div>

                  </div>
                  <div class="form-group row">
                      <div class="col-xs-6">
                        <label for="ex3">Column No</label>
                        <input class="form-control" id="ex3" type="text" placeholder="1,3,5,7" name="column_no">
                      </div>
                      <div class="col-xs-6">
                        <label for="ex3">Total Seat</label>
                        <input class="form-control" id="ex3" type="text" placeholder="Ex: 40" name="total_seat">
                      </div>
                  </div>
                  <button type="submit" class="btn btn-default" name="submit">Submit</button>
                </form>
            </div>
            <div class="col-md-3">
            </div>
        </div>
    <!--Body End-->
    <!--Edit action -->
    <?php
    require_once 'manage_seatplan_validaton.php';
    $result=seatplan();

    //        Showing messages

    if ($result)
    {
        if ($result['result']==0)
        {
            ?>
            <div class="col-md-4">
            </div>

            <div class="col-md-4 row_margin row_margin_bottom">
                <div class="alert alert-success" role="alert">
                    <?php echo $result['message']; ?>
                </div>
            </div>
            <?php
        }
        else
        {
            ?>

            <div class="col-md-4">
            </div>

            <div class="col-md-4 row_margin row_margin_bottom">
                <div class="alert alert-danger" role="alert">
                    <strong>Error!</strong> <?php echo $result['message']; ?>
                </div>
            </div>

            <?php
        }
    }
    ?>


    <!-- Navbar bottom-->
	<?php include('../includes/footer.php'); ?>
	<!--JavaScript here-->
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

    <script  src="../js/index.js"></script>

</body>
</html>
<?php }}?>