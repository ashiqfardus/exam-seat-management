<?php
/**
 * Created by PhpStorm.
 * User: Ashiq Fardus
 * Date: 5/31/2018
 * Time: 1:13 AM
 */
require '../includes/connection.php';
if (!isset($_SESSION))
{
    session_start();
}
if(!isset($_SESSION['email']))
{
    header("location: admin_login.php");
}
else
{
    if (isset($_GET['id']))
    {
        $id=$_GET['id'];
        $sql="DELETE from semester where semester_id='$id'";
        if (mysqli_query($connection,$sql))
        {
            ?>
            <script>
                window.location="view_semester.php";
            </script>
            <?php
        }
    }
}