<?php include '../includes/connection.php';
session_start();
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Exam Seat Handling</title>
    <link rel='stylesheet prefetch' href='../css/bootstrap.css'>
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/custom.css">

  
</head>

<body>
	<!-- Navbar -->
    
	<?php include('../includes/admin_nav.php'); ?>
	
	<!-- Login and Registration Form-->
    <div class="container row_margin_bottom">
        <div class="col-md-6 col-md-offset-3 col-xs-12">
            <!-- @ Start login box wrapper -->
            <div class="blmd-wrapp">
                <div class="blmd-color-conatiner ripple-effect-All"></div>
                <div class="blmd-header-wrapp ">
                    <?php
                    $sql= mysqli_query($connection,"SELECT * FROM admin");
                    if($row=mysqli_num_rows($sql)==0) {
                        ?>
                        <div class="blmd-switches">
                            <button class="btn btn-circle btn-lg btn-blmd ripple-effect btn-success blmd-switch-button">
                                <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                                    <path fill="#fff" d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z"/>
                                </svg>
                            </button>
                        </div>

                        <?php
                    }
                    ?>

                </div>
                <div class="blmd-continer">
                    <!-- Login form container -->
                    <form action="admin_login.php" class="clearfix" id="login-form" method="post">
                        <h1>Admin Login</h1>
                        <div class="col-md-12">

                            <div class="input-group blmd-form">
                                <div class="blmd-line">
                                    <input type="text" name="email" autocomplete="off" id="username" class="form-control">
                                    <label class="blmd-label">Email or Username</label>
                                </div>
                            </div>
                            <div class="input-group blmd-form">
                                <div class="blmd-line">
                                    <input type="password" name="password" autocomplete="off" id="password" class="form-control">
                                    <label class="blmd-label">Password</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 text-center">
                            <button type="submit" name="login" class="btn btn-blmd ripple-effect btn-success btn-lg btn-block">Login</button>
                        </div>
                        <br/>
                    </form>


                    <!-- Registration form -->




                        <form action="admin_login.php" class="clearfix form-hidden" id="Register-form" method="post">
                            <h1>Admin SignUp</h1>
                            <div class="col-md-12">

                                <div class="input-group blmd-form">
                                    <div class="blmd-line">
                                        <input type="text" name="username" autocomplete="off" id="username"
                                               class="form-control">
                                        <label class="blmd-label">Username</label>
                                    </div>
                                </div>
                                <div class="input-group blmd-form">
                                    <div class="blmd-line">
                                        <input type="email" name="email" autocomplete="off" id="username"
                                               class="form-control">
                                        <label class="blmd-label">Email</label>
                                    </div>
                                </div>
                                <div class="input-group blmd-form">
                                    <div class="blmd-line">
                                        <input type="password" name="password" autocomplete="off" id="password"
                                               class="form-control">
                                        <label class="blmd-label">Password</label>
                                    </div>
                                </div>
                                <div class="input-group blmd-form">
                                    <div class="blmd-line">
                                        <input type="password" name="Password2" autocomplete="off" id="password"
                                               class="form-control">
                                        <label class="blmd-label">Repeat Password</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 text-center">
                                <button type="submit" name="submit"
                                        class="btn btn-blmd ripple-effect btn-warning btn-lg btn-block">Register
                                </button>
                            </div>
                            <br/>
                        </form>


                </div>
            </div>

        </div>
    </div>
		<!-- Login and registration form end here-->

    <!-- Login action here-->
    <?php
    require_once 'admin_login_validation.php';
    $result=alogin();

    //        Showing messages

    if ($result)
    {
        if ($result['result']==0)
        {
            ?>
            <script>
                window.location.href='admin_homepage.php';
            </script>
        <?php
        }
        else
        {
        ?>
            <div class="col-md-4">

            </div>
            <div class="col-md-4">
                <div class="alert alert-danger" role="alert">
                    <strong>Error!</strong> <?php echo $result['message']; ?>
                </div>
            </div>
            <?php
        }
    }
    ?>

    <!--        Sign-up action-->
    <?php
    require_once 'admin_reg_validation.php';
    $result=singup_fn();

    //        Showing messages

    if ($result)
    {
        if ($result['result']==0)
        {
            ?>
            <div class="col-md-4">

            </div>
            <div class="col-md-4">
                <div class="alert alert-success" role="alert">
                    <strong>Success!</strong> <?php echo $result['message']; ?>
                </div>
            </div>
            <?php
        }
        else
        {
            ?>
            <div class="col-md-4">

            </div>
            <div class="col-md-4">
                <div class="alert alert-danger" role="alert">
                    <strong>Error!</strong> <?php echo $result['message']; ?>
                </div>
            </div>
            <?php
        }
    }
    ?>


        <!-- Navbar bottom-->
		<?php include('../includes/footer.php'); ?>
		<!--JavaScript here-->
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

    <script  src="../js/index.js"></script>

</body>
</html>
