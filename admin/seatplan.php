<?php
/**
 * Created by PhpStorm.
 * User: Ashiq Fardus
 * Date: 4/21/2018
 * Time: 5:08 PM
 */
require '../includes/connection.php';
if (!isset($_SESSION))
{
    session_start();
}
if(!isset($_SESSION['email']))
{
    header("location: admin_login.php");
}
else
{
    $email=$_SESSION['email'];
    $query="SELECT * FROM admin WHERE email= '$email' OR username='$email'";
    $run=mysqli_query($connection,$query);
    while ($data=mysqli_fetch_array($run))
    {
        $id=$data['a_id'];
        $username=$data['username'];
        $email=$data['email'];
        ?>
        <!DOCTYPE html>
        <html >
        <head>
            <meta charset="UTF-8">
            <title>Exam Seat Handling</title>
            <link rel='stylesheet prefetch' href='../css/bootstrap.css'>
            <link rel="stylesheet" href="../css/style.css">
            <link rel="stylesheet" href="../css/custom.css">
            <link rel="stylesheet" href="../css/font-awesome.min.css">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

        </head>

        <body>
        <!-- Navbar top -->
        <?php include('../includes/admin_home_nav.php'); ?>
        <!-- Navbar end here-->

        <div class="row_margin_h">
            <h2 class="header_text"> Welcome to Exam Seat Management System</h2>
        </div>
        <div class="col-md-2">

        </div>

        <div class="col-md-8 column_color row_margin_bottom">
            <h3 class="header_text">Seatplan</h3>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Date</th>
                        <th>Room No</th>
                        <th>Time</th>
                        <th>Course Code</th>
                        <th>Section</th>
                        <th>Column No</th>
                        <th>Total Seat</th>
                        <th>Teacher1</th>
                        <th>Teacher2</th>
                        <th>Semester</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php $query=mysqli_query($connection,"SELECT * FROM room_details");
                        while($row=mysqli_fetch_array($query))
                        {
                            $id=$row['r_id'];
                            $room_no=$row['room_no'];
                            $time=$row['time'];
                            $teacher1=$row['teacher1'];
                            $teacher2=$row['teacher2'];
                            $course_code=$row['course_code'];
                            $section=$row['section'];
                            $column=$row['column_no'];
                            $total_seat=$row['total_seat'];
                            $semester=$row['semester'];
                            $date=$row['date'];
                            ?>
                            <tr>
                                <td><?php echo $id ?></td>
                                <td><?php echo $date ?></td>
                                <td><?php echo $room_no?></td>
                                <td><?php echo $time?></td>
                                <td><?php echo $course_code?></td>
                                <td><?php echo $section?></td>
                                <td><?php echo $column?></td>
                                <td><?php echo $total_seat?></td>
                                <td><?php echo $teacher1?></td>
                                <td><?php echo $teacher2?></td>
                                <td><?php echo $semester?></td>
                                <td><a href="delete_seatplan.php?id=<?php echo $id?>" class="btn btn-danger btn-group-sm"><i class="fas fa-trash-alt"></i></a></td>
                            </tr>
                        <?php } ?>

                    </tbody>


                </table>
            </div>
        </div>
        <div class="col-md-2">

        </div>



        <!--NAavbar bottom-->

        <?php include('../includes/footer.php'); ?>

        <!--JavaScript here-->
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

        <script  src="../js/index.js"></script>

        </body>
        </html>
    <?php }} ?>