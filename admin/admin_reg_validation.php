<?php
/**
 * Created by PhpStorm.
 * User: Ashiq Fardus
 * Date: 2/21/2018
 * Time: 3:41 PM
 */
require '../includes/connection.php';

function singup_fn()
{
    global $connection;
//    Using result for error messages
    $result=array(
        'result'=>'1',
        'message'=>'Undefined Error'
    );
//    Form Validation
    if (isset($_POST['submit']))
    {
        $username=$_POST['username'];
        $email=$_POST['email'];
        $password=$_POST['password'];
        $password2=$_POST['password'];

//        Email check if exists or not
        $emailcheck=mysqli_query($connection,"Select email from admin WHERE email='$email'");
        $echeck=mysqli_num_rows($emailcheck);
        if ($echeck!=0)
        {
            $result['message']='Email already exists';
            return $result;
        }

        //initial check
        $query=mysqli_query($connection,"SELECT username FROM admin WHERE username='$username'");
        $initial_check=mysqli_num_rows($query);
        if ($initial_check!=0)
        {
            $result['message']='Username is not available. Try another';
            return $result;
        }

//        Checking empty field
        if (empty($username)|| empty($email)|| empty($password)||empty($password2))
        {
            $result['message']='Any of the field is empty';
            return $result;
        }

//        Password length check
        if (strlen($password)>20 || strlen($password)<6)
        {
            $result['message']='Password length must be between 6-20 character';
            return $result;
        }

//        Password matching
        if ($password != $password2)
        {
            $result['message']="Password didn't match";
            return $result;
        }
        $password=md5($password);
        $query="INSERT INTO admin(username,email,password) VALUES ('$username','$email','$password')";
        if (mysqli_query($connection,$query))
        {
            $result['result']=0;
            $result['message']='Registration completed. Please Login Now.';
            return $result;
        }
    }
}