<?php
/**
 * Created by PhpStorm.
 * User: Ashiq Fardus
 * Date: 3/11/2018
 * Time: 4:31 PM
 */
include 'includes/connection.php';

function sedit()
{
    global $connection;
    $result=array(
        'result'=>'1',
        'message'=>'Undefined Message'
    );
    if (isset($_POST['submit']))
    {
        $update_email=$_GET['update_id'];
        $image=$_FILES['image']['name'];
        $image_tmp=$_FILES['image']['tmp_name'];
        $name=$_POST['name'];
        $phone=$_POST['phone'];
        $gender=$_POST['gender'];
        $dob=$_POST['dob'];



        //Image field check
        if (empty($image))
        {
            $result['message']='Image is required';
            return $result;
        }

        //Checking the file is image or not
        $check = getimagesize($image_tmp);
        if ($check==FALSE)
        {
            $result['message']='The file is not an image';
            return $result;
        }

        //Name
        if (empty($name))
        {
            $result['message']='Name is required';
            return $result;
        }

        //uploading image
        move_uploaded_file($image_tmp,"image/$image");

        $update_query="UPDATE student_information SET name='$name',
                                          image='$image',
                                          phone='$phone',
                                          gender='$gender',
                                          dob='$dob'
                                          WHERE id='$update_email'";

        if(mysqli_query($connection,$update_query))
        {
            $result['result']=0;
            $result['message']='Profile successfully updated.';
            return $result;
        }
    }
}

?>