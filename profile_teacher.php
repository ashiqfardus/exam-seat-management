<?php
include 'includes/connection.php';
if (!isset($_SESSION))
{
    session_start();
}
if(!isset($_SESSION['email']))
{
    header("location: u_log.php");
}
else
{
$email=$_SESSION['email'];
    $query="SELECT * FROM teacher WHERE email= '$email'";
    $run=mysqli_query($connection,$query);
    while ($data=mysqli_fetch_array($run))
    {
        $id=$data['t_id'];
        //$email=$data['email'];
        $name=$data['name'];
        $dob=$data['dob'];
        $phone=$data['phone'];
        $gender=$data['gender'];
        $initial=$data['initial'];
        $image=$data['image'];
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Exam Seat Handling</title>
    <link rel='stylesheet prefetch' href='css/bootstrap.css'>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
  
</head>

<body>
	<!-- Navbar top -->
		<?php include('includes/teacher_nav.php'); ?>
	<!-- Navbar end here-->



	<!--Page Body-->
	    <div class="container row_margin_h">
	    	<div class="row row_margin_bottom">
	    		<div class="col-md-12 column_color button_alignment">
	    			<img class="img-circle" style="height: 200px; width: 200px;" src="image/<?php if (isset($image)){ echo $image; } ?>">

	    			<p class="p_text">Name: <?php if (isset($name)){ echo $name; } ?> </p>
	    			<p class="p_text">Initial: <?php if (isset($initial)){ echo $initial; } ?> </p>
	       			<p class="p_text">Email: <?php if (isset($email)){ echo $email; } ?> </p>
	       			<p class="p_text">Phone Number: 0<?php if (isset($phone)){ echo $phone; } ?> </p>
	       			<p class="p_text">Gender: <?php if (isset($gender)){ echo $gender; } ?> </p>
	    		</div>
	    	</div>
	    </div>
    <!-- Body End-->


    <!--NAavbar bottom-->
    
	<?php include('includes/footer.php'); ?>
		
		<!--JavaScript here-->
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

    <script  src="js/index.js"></script>

</body>
</html>
<?php } }?>