-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 18, 2018 at 04:56 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `esm`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `a_id` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`a_id`, `username`, `email`, `password`, `date`) VALUES
(1, 'tonoyshil', 'tonoy@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '2018-05-15 15:58:49'),
(2, 'admin', 'admin@diu.edu.bd', 'e10adc3949ba59abbe56e057f20f883e', '2018-07-29 20:41:03');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `c_id` int(11) NOT NULL,
  `course_code` varchar(15) NOT NULL,
  `student_id` varchar(15) NOT NULL,
  `semester` varchar(25) NOT NULL,
  `section` varchar(10) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`c_id`, `course_code`, `student_id`, `semester`, `section`, `time`) VALUES
(1, 'CSE 424', '143-15-4525', 'Fall 2018', 'a', '2018-07-11 15:44:49'),
(2, 'CSE 423', '143-15-4525', 'Fall 2018', 'a', '2018-07-11 15:44:49'),
(3, 'CSE 422', '143-15-4525', 'Summer 2018', 'a', '2018-07-11 15:44:49'),
(5, 'CSE 422', '143-15-4525', 'Fall 2018', 'a', '2018-07-11 15:44:49'),
(6, 'cse 424', '143-15-4630', 'Spring 2019', 'a', '2018-07-11 15:44:49'),
(7, 'CSE 422', '143-15-4630', 'Fall 2018', 'a', '2018-07-11 15:44:49'),
(8, 'cse 422', '143-15-2345', 'Spring 2019', 'c', '2018-07-29 21:07:54'),
(9, 'cse 423', '143-15-2345', 'Spring 2019', 'c', '2018-07-29 21:08:05'),
(10, 'cse 422', '143-15-4639', 'Spring 2019', 'c', '2018-07-29 21:34:41'),
(11, 'cse 423', '143-15-4639', 'Spring 2019', 'c', '2018-07-29 21:34:47');

-- --------------------------------------------------------

--
-- Table structure for table `exam_hall_details`
--

CREATE TABLE `exam_hall_details` (
  `id` int(11) NOT NULL,
  `room_no` varchar(20) NOT NULL,
  `student_id` varchar(30) NOT NULL,
  `course_code` varchar(20) NOT NULL,
  `section` varchar(10) NOT NULL,
  `semester_no` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_hall_details`
--

INSERT INTO `exam_hall_details` (`id`, `room_no`, `student_id`, `course_code`, `section`, `semester_no`, `time`, `date`) VALUES
(4, '101 CSE', '143-15-4630', 'CSE 422', 'a', 'Fall 2018', '10.00AM-11.30AM', '2018-07-08'),
(11, '101 CSE', '143-15-4525', 'CSE 424', 'a', 'Fall 2018', '10.00AM-11.30AM', '2018-07-08'),
(12, '101 CSE', '143-15-4525', 'CSE 423', 'a', 'Fall 2018', '10.00AM-11.30AM', '2018-07-08');

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE `room` (
  `id` int(11) NOT NULL,
  `room_no` varchar(15) NOT NULL,
  `capacity` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`id`, `room_no`, `capacity`) VALUES
(1, '102 CSE', 41),
(3, '101 CSE', 41),
(4, '801 CSE', 42);

-- --------------------------------------------------------

--
-- Table structure for table `room_details`
--

CREATE TABLE `room_details` (
  `r_id` int(11) NOT NULL,
  `room_no` varchar(15) NOT NULL,
  `time` varchar(50) NOT NULL,
  `teacher1` varchar(20) NOT NULL,
  `teacher2` varchar(20) DEFAULT NULL,
  `course_code` varchar(25) NOT NULL,
  `section` varchar(10) NOT NULL,
  `column_no` varchar(30) NOT NULL,
  `total_seat` int(100) NOT NULL,
  `remaining_seat` int(100) NOT NULL,
  `semester` varchar(30) NOT NULL,
  `date` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_details`
--

INSERT INTO `room_details` (`r_id`, `room_no`, `time`, `teacher1`, `teacher2`, `course_code`, `section`, `column_no`, `total_seat`, `remaining_seat`, `semester`, `date`) VALUES
(4, '101 CSE', '10.00AM-11.30AM', 'TS', '', 'CSE 423', 'a', '1,3,5,7', 10, 2, 'Fall 2018', '2018-07-08'),
(6, '101 CSE', '10.00AM-11.30AM', 'AS', '', 'CSE 422', 'a', '2,4,6,8', 31, 25, 'Fall 2018', '2018-07-08'),
(7, '801 CSE', '12.00-1.30PM', 'TS', 'AS', 'CSE 422', 'c', '1,3,5,7', 20, 20, 'Summer 2019', '2018-07-13'),
(8, '801 CSE', '10.00AM-11.30AM', 'MAH', '', 'CSE 422', 'c', '1,3,5,7', 20, 20, 'Spring 2019', '2018-07-13'),
(9, '801 CSE', '4.00PM-5.30PM', 'MAH', 'AS', 'CSE 422', 'a', '2,4,6,8', 12, 6, 'Summer 2018', '2018-07-28'),
(10, '101 CSE', '4.00PM-5.30PM', 'MAH', 'TS', 'CSE 424', 'c', '1,3,5,7', 20, 20, 'Fall 2019', '2019-09-12');

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE `semester` (
  `semester_id` int(11) NOT NULL,
  `semester_no` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`semester_id`, `semester_no`) VALUES
(1, 'Fall 2019'),
(2, 'Fall 2018'),
(3, 'Spring 2018'),
(4, 'Summer 2018'),
(5, 'Spring 2019'),
(8, 'Summer 2019'),
(10, 'Fall 2020');

-- --------------------------------------------------------

--
-- Table structure for table `student_information`
--

CREATE TABLE `student_information` (
  `id` int(11) NOT NULL,
  `student_id` varchar(30) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `rfid_tag` varchar(255) NOT NULL,
  `semester` varchar(30) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `gender` varchar(15) NOT NULL,
  `dob` varchar(50) NOT NULL,
  `image` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_information`
--

INSERT INTO `student_information` (`id`, `student_id`, `name`, `email`, `password`, `rfid_tag`, `semester`, `phone`, `gender`, `dob`, `image`, `date`) VALUES
(1, '143-15-4525', 'Tonoy', 'tonoy@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'ashiq', 'Fall 2018', '01545484898', 'Male', '2014-07-11', '2.JPG', '2018-05-15 16:08:00'),
(2, '143-15-4630', 'ashiq', 'ashiq@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'ashik', 'Fall 2018', '01726113015', 'Male', '2018-07-17', 'Blank Diagram (1).jpeg', '2018-05-15 16:15:30'),
(3, '143-15-2345', 'Nadim Hasan', 'nadim2345@diu.edu.bd', 'c33367701511b4f6020ec61ded352059', 'ASDFGH', 'Spring 2019', '017Xxxxxxxx', 'Male', '2018-07-17', 'asd.jpg', '2018-07-29 21:00:20'),
(7, '143-15-4639', 'Md. Asikul Islam', 'asikul4639@diu.edu.bd', 'e10adc3949ba59abbe56e057f20f883e', 'DemoRFID', 'Spring 2019', '017Xxxxxxxx', 'Male', '2018-07-21', 'asd.jpg', '2018-07-29 21:34:03');

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE `teacher` (
  `t_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `initial` varchar(15) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `gender` varchar(15) NOT NULL,
  `dob` varchar(50) NOT NULL,
  `image` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`t_id`, `email`, `password`, `initial`, `name`, `phone`, `gender`, `dob`, `image`, `date`) VALUES
(1, 'tonoy@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'TS', 'Tonoy Bihari', '018888888888', 'Male', '1998-05-22', 'asd.jpg', '2018-05-15 16:01:47'),
(2, 'ashiq@gmail.com', '123456', 'AS', 'Ashiq', '01726113015', 'male', '', '', '2018-06-24 20:02:51'),
(3, 'azizul.cse@diu.edu.bd', 'c33367701511b4f6020ec61ded352059', 'MAH', 'Md. Azizul Hakim', '017Xxxxxxxx', 'Male', '2018-07-13', '68e2fcf0a4b527f58cf58de71fc4b04e.JPG', '2018-07-29 20:45:24');

-- --------------------------------------------------------

--
-- Table structure for table `time`
--

CREATE TABLE `time` (
  `id` int(11) NOT NULL,
  `time_list` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `time`
--

INSERT INTO `time` (`id`, `time_list`) VALUES
(5, '10.00AM-11.30AM'),
(8, '4.00PM-5.30PM');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `exam_hall_details`
--
ALTER TABLE `exam_hall_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_details`
--
ALTER TABLE `room_details`
  ADD PRIMARY KEY (`r_id`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`semester_id`);

--
-- Indexes for table `student_information`
--
ALTER TABLE `student_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`t_id`);

--
-- Indexes for table `time`
--
ALTER TABLE `time`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `a_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `exam_hall_details`
--
ALTER TABLE `exam_hall_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `room_details`
--
ALTER TABLE `room_details`
  MODIFY `r_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `semester`
--
ALTER TABLE `semester`
  MODIFY `semester_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `student_information`
--
ALTER TABLE `student_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `teacher`
--
ALTER TABLE `teacher`
  MODIFY `t_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `time`
--
ALTER TABLE `time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
