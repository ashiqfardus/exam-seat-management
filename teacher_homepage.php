
<?php
include 'includes/connection.php';
if (!isset($_SESSION))
{
    session_start();
}
if(!isset($_SESSION['email']))
{
    header("location: u_log.php");
}
else
{
$email=$_SESSION['email'];
    $query="SELECT * FROM teacher WHERE email= '$email'";
    $run=mysqli_query($connection,$query);
    while ($data=mysqli_fetch_array($run))
    {
        $id=$data['t_id'];
        //$email=$data['email'];
        $name=$data['name'];
        $dob=$data['dob'];
        $phone=$data['phone'];
        $initial=$data['initial'];
        $image=$data['image'];
        $gender=$data['gender'];
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Exam Seat Handling</title>
    <link rel='stylesheet prefetch' href='css/bootstrap.css'>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/custom.css">
    <script src="https://use.fontawesome.com/6e655bd209.js"></script>
  
</head>

<body>
	<!-- Navbar top -->
		<?php include('includes/teacher_nav.php'); ?>
	<!-- Navbar end here-->



	<!--Page Body-->
	    <div class="container">
	    	<!-- Header Row-->
	    	<div class="row row_margin_home">
	    		<h2 class="header_text"> Welcome to Exam Seat Management System</h2>
	    	</div>

	    	<!-- Details Row-->
	       <div class="row row_margin">
	       		<!-- Details part-->
	       		<div class="col-md-5 column_color">
	       			<div class="row">
	       				<h3 class="header_text">Details</h3>
	       			</div>
	       			<div class="row row_margin">
	       				<p class="p_text">Name: <?php echo $name?> </p>
		    			<p class="p_text">Initial: <?php echo $initial?> </p>
		       			<p class="p_text">Email: <?php echo $email?> </p>
		       			<p class="p_text">Phone Number: <?php echo $phone?> </p>
		       			<p class="p_text">Gender: <?php echo $gender?> </p>
	       			</div>
	       		</div>
	       		<div class="col-md-1">
	       		</div>


	       		<!-- Course and section part-->
	       		<div class="col-md-6 column_color">
	       			<h3 class="header_text">Exam Duty</h3>
	       			<div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Course Code</th>
                                <th>Room No</th>
                                <th>Semester</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sql=mysqli_query($connection,"SELECT * FROM room_details where teacher1='$initial' or teacher2='$initial'");
                            while ($res=mysqli_fetch_array($sql))
                            {
                                $r_id=$res['r_id'];
                                $room_no=$res['room_no'];
                                $time=$res['time'];
                                $course_codef=$res['course_code'];
                                $sectionf=$res['section'];
                                $column_no=$res['column_no'];
                                $total_seat=$res['total_seat'];
                                $semesterf=$res['semester'];
                                $date=$res['date'];
                                $teacher1=$res['teacher1'];
                                $teacher2=$res['teacher2'];

                            ?>
                            <tr>
                                <td><?php echo $r_id?></td>
                                <td><?php echo $date?></td>
                                <td><?php echo $time?></td>
                                <td><?php echo $course_codef?></td>
                                <td><?php echo $room_no?></td>
                                <td><?php echo $semesterf?></td>
                            </tr>
                            <?php }?>
                            </tbody>
                        </table>
					</div>
	       		</div>


	       </div>
	       <!-- Details row end-->

	       <div class="row row_margin row_margin_bottom">
	       		<div class="col-md-12 column_color">
	       			<h3 class="button_alignment">Instructions </h3>
	       			<p class="p_text"> 1. At first edit your profile and Initial.</p>
	       			<p class="p_text"> 2. If you enter any worng data then authority will not be responsible. </p>
	       			<p class="p_text"> 3. Provide your personal information correctly </p>
	       		</div>
	       </div>


	    </div>
    <!-- Body End-->


    <!--NAavbar bottom-->
    
    <?php include('includes/footer.php'); ?>
		
		<!--JavaScript here-->
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

    <script  src="js/index.js"></script>

</body>
</html>
<?php } }?>