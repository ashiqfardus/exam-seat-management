<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Exam Seat Handling</title>
    <link rel='stylesheet prefetch' href='css/bootstrap.css'>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/custom.css">

  
</head>

<body>
	<!-- Navbar top -->
	<?php include('includes/login_nav.php'); ?>
	<!--Page Body-->
	    <div class="container-fluid">
	        <div class="row row_margin_h">
	            <h1 class="header_text"> Welcome to The Exam Seat Management System </h1>
	            <h2 class="header_text "> Login As  </h2>
	        </div>
	        <div class="row row_margin">
	        		<div class="button_alignment">

                        <a href="admin/admin_login.php" class="btn button_info btn-lg" role="button">Admin</a>
	        		</div>
	        </div>
	        <div class="row row_margin">
	        		<div class="button_alignment">
	        			<a href="teacher_login.php" class="btn button_info btn-lg" role="button">Teacher</a>
	        		</div>
	        </div>
            <div class="row row_margin">
                <div class="button_alignment">
                    <a href="student_login.php" class="btn button_info btn-lg" role="button">Student</a>
                </div>
            </div>
	    </div>
    <!-- Body End-->


    <!--NAavbar bottom-->
    	<?php include('includes/footer.php'); ?>
		<!--JavaScript here-->
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

    <script  src="js/index.js"></script>

</body>
</html>
