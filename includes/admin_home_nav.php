<?php require 'connection.php';
if (!isset($_SESSION))
{
    session_start();
}
if(!isset($_SESSION['email']))
{
    header("location: admin_login.php");
}
else
{
$email=$_SESSION['email'];
$query="SELECT * FROM admin WHERE email= '$email' OR username='$email'";
$run=mysqli_query($connection,$query);
while ($data=mysqli_fetch_array($run))
{
$id=$data['a_id'];
$username=$data['username'];
$email=$data['email'];
?>

<div class="container-fluid">
		<nav class="navbar navbar-default nav_color navbar-fixed-top">
		  <div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="../admin/admin_homepage.php">ESH</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <ul class="nav navbar-nav">
				<li><a href="../admin/seatplan.php">Seatplan <span class="sr-only">(current)</span></a></li>
				<li><a href="../admin/manage_seatplan.php">Manage Seatplan</a></li>
				<li><a href="../admin/view_room.php">Room</a></li>
				<li><a href="../admin/add_room.php">Add Room</a></li>
				<li><a href="../admin/view_semester.php">Semester</a></li>
				<li><a href="../admin/add_semester.php">Add Semester</a></li>
                  <li><a href="../admin/view_time.php">Time Table</a></li>
                  <li><a href="../admin/add_time.php">Add Time</a></li>
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li><a href="../admin/profile_admin.php"><?php echo $username ?></a></li>
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu <span class="caret"></span></a>
				  <ul class="dropdown-menu">
				  	<li><a href="../admin/profile_admin.php?id=<?php echo $id ?>"><i class="fa fa-user" aria-hidden="true"></i> profile</a></li>
					<li><a href="../admin/edit_admin_profile.php?id=<?php echo $id ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Profile</a></li>
					<li><a href="#"><i class="fa fa-list-ol" aria-hidden="true"></i> View Seatplan</a></li>
					<li><a href="../admin/manage_seatplan.php"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Manage Seatplan</a></li>
					<li><a href="../admin/admin_pass_change.php?id=<?php echo $id ?>"><i class="fa fa-unlock-alt" aria-hidden="true"></i> Change Password</a></li>
					<li role="separator" class="divider"></li>
					
					<li><a href="../logout.php"><i class="fa fa-user" aria-hidden="true"></i> Logout</a></li>
				  </ul>
				</li>
			  </ul>
			</div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</div>
	<script src="https://use.fontawesome.com/6e655bd209.js"></script>

<?php } } ?>