<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Exam Seat Handling</title>
    <link rel='stylesheet prefetch' href='css/bootstrap.css'>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
  
</head>

<body>
	<!-- Navbar top -->
		<?php include('includes/student_nav.php'); ?>
	<!-- Navbar end here-->



	<!--Page Body-->
	    <div class="container row_margin_h row_margin_bottom">
	    	<h1 class="button_alignment" style="margin-bottom: 20px;"> Update your course registration. </h2>
	    	<div class="col-md-3">
	    	</div>
	    	<div class="col-md-6 column_color">
	    		<form>
	    			<div class="form-group">
					  <label for="sel1">Select list:</label>
					  <select class="form-control" id="sel1">
					    <option>Fall 2017</option>
					    <option>Summer 2017</option>
					    <option>Spring 2017</option>
					    <option>Fall 2016</option>
					  </select>
					</div>
				  <div class="form-group row">
				      <div class="col-xs-6">
				        <label for="ex3">Course</label>
				        <input class="form-control" id="ex3" type="text">
				      </div>
				      <div class="col-xs-6">
				        <label for="ex3">Section</label>
				        <input class="form-control" id="ex3" type="text">
				      </div>
				  </div>
				  <div class="form-group row">
				      <div class="col-xs-6">
				        <label for="ex3">Course</label>
				        <input class="form-control" id="ex3" type="text">
				      </div>
				      <div class="col-xs-6">
				        <label for="ex3">Section</label>
				        <input class="form-control" id="ex3" type="text">
				      </div>
				  </div>
				  <div class="form-group row">
				      <div class="col-xs-6">
				        <label for="ex3">Course</label>
				        <input class="form-control" id="ex3" type="text">
				      </div>
				      <div class="col-xs-6">
				        <label for="ex3">Section</label>
				        <input class="form-control" id="ex3" type="text">
				      </div>
				  </div>
				  <div class="form-group row">
				      <div class="col-xs-6">
				        <label for="ex3">Course</label>
				        <input class="form-control" id="ex3" type="text">
				      </div>
				      <div class="col-xs-6">
				        <label for="ex3">Section</label>
				        <input class="form-control" id="ex3" type="text">
				      </div>
				  </div>
				  <div class="form-group row">
				      <div class="col-xs-6">
				        <label for="ex3">Course</label>
				        <input class="form-control" id="ex3" type="text">
				      </div>
				      <div class="col-xs-6">
				        <label for="ex3">Section</label>
				        <input class="form-control" id="ex3" type="text">
				      </div>
				  </div>
				  <button type="submit" class="btn btn-default">Submit</button>
				</form>
	    	</div>
	    	<div class="col-md-3">
	    	</div>
	    </div>
    <!-- Body End-->


    <!--NAavbar bottom-->
    
	<?php include('includes/footer.php'); ?>
		
		<!--JavaScript here-->
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

    <script  src="js/index.js"></script>

</body>
</html>
