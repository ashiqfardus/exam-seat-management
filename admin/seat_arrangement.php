<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Exam Seat Handling</title>
    <link rel='stylesheet prefetch' href='../css/bootstrap.css'>
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/custom.css">
</head>

<body>
    <!-- Navbar top -->

    <div class="row">
            <nav class="navbar navbar-default nav_color navbar-fixed-top">
                <h3 style="color: white; text-align: center; font-weight: bold;">Exam Seat Management</h3>
            </nav>
    </div>
    <!--Page Body-->

        <div class="container row_margin_h">
            <div class="col-md-12 col-sm-12">
                <div class="row">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Room No</th>
                            <th>Time</th>
                            <th>Exam invigilator</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>901 CSE</td>
                            <td>10.00-11.30</td>
                            <td>MAH & RAS</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row row_margin">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Course Code</th>
                            <th>Section</th>
                            <th>Column</th>
                            <th>Total Seat</th>
                            <th>Remaining Seat</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>CSE 314</td>
                            <td>A,B,C,D</td>
                            <td>1,3,5,7</td>
                            <td>20</td>
                            <td>15</td>
                        </tr>
                        <tr>
                            <td>CSE 414</td>
                            <td>A,B,C,D</td>
                            <td>2,4,6,8</td>
                            <td>20</td>
                            <td>15</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row row_margin">
                    <h3 class="button_alignment" style="font-weight: bold;">Scan Your RFID to Enter into The Room</h3>
<!--                    <h3 class="button_alignment" style="font-weight: bold;">Enter Your ID for Enrollment</h3>-->
<!--                    <form>-->
<!--                        <div class="form-group row">-->
<!--                            <div class="col-xs-3">-->
<!--                            </div>-->
<!--                            <div class="col-xs-5">-->
<!--                                <input class="form-control button_alignment" id="ex3" type="text" placeholder="143-15-4639">-->
<!--                            </div>-->
<!--                            <div class="col-xs-3">-->
<!--                                <button type="submit" class="btn btn-default">Submit</button>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </form>-->
                </div>
            </div>
        </div>

<!-- Body End-->


<!--NAavbar bottom-->
<?php include('../includes/footer.php'); ?>
<!--JavaScript here-->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

<script  src="../js/index.js"></script>

</body>
</html>
