<?php
/**
 * Created by PhpStorm.
 * User: Ashiq Fardus
 * Date: 4/21/2018
 * Time: 4:59 PM
 */
require '../includes/connection.php';

function add_semester()
{
    global $connection;
//    Using result for error messages
    $result=array(
        'result'=>'1',
        'message'=>'Undefined Error'
    );
//    Form Validation
    if (isset($_POST['submit']))
    {
        $semester=$_POST['semester'];

//        Semester check if exists or not
        $room_check=mysqli_query($connection,"Select semester_no from semester WHERE semester_no='$semester'");
        $rcheck=mysqli_num_rows($room_check);
        if ($rcheck!=0)
        {
            $result['message']='Semester already exists';
            return $result;
        }


//        Checking empty field
        if (empty($semester))
        {
            $result['message']='The field is empty';
            return $result;
        }


        $query="INSERT INTO semester(semester_no) VALUES ('$semester')";
        if (mysqli_query($connection,$query))
        {
            $result['result']=0;
            $result['message']='Semester has been added';
            return $result;
        }
    }
}
function update_semester()
{
    global $connection;
//    Using result for error messages
    $result=array(
        'result'=>'1',
        'message'=>'Undefined Error'
    );

    //Form Validation
    if (isset($_POST['submit']))
    {
        $id=$_GET['id'];
        $semester=$_POST['semester'];

//        Semester check if exists or not
        $room_check=mysqli_query($connection,"Select semester_no from semester WHERE semester_no='$semester'");
        $rcheck=mysqli_num_rows($room_check);
        if ($rcheck!=0)
        {
            $result['message']='Semester already exists';
            return $result;
        }


//        Checking empty field
        if (empty($semester))
        {
            $result['message']='The field is empty';
            return $result;
        }


        $query="UPDATE semester SET semester_no='$semester' where semester_id='$id'";
        if (mysqli_query($connection,$query))
        {
            $result['result']=0;
            $result['message']='Semester has been updated';
            return $result;
        }
    }
}