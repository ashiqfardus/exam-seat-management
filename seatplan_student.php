<?php
include 'includes/connection.php';
if (!isset($_SESSION))
{
    session_start();
}
if(!isset($_SESSION['email']))
{
    header("location: u_log.php");
}
else
{
    $email=$_SESSION['email'];
    $query="SELECT * FROM student_information WHERE email= '$email' OR student_id='$email'";
    $run=mysqli_query($connection,$query);
    while ($data=mysqli_fetch_array($run))
    {
        $id=$data['id'];
        $image=$data['image'];
        $student_id=$data['student_id'];
        $name=$data['name'];
        $phone=$data['phone'];
        $dob=$data['dob'];
        $gender=$data['gender'];
        $semester=$data['semester'];
        ?>
        <!DOCTYPE html>
        <html >
        <head>
            <meta charset="UTF-8">
            <title>Exam Seat Handling</title>
            <link rel='stylesheet prefetch' href='css/bootstrap.css'>
            <link rel="stylesheet" href="css/style.css">
            <link rel="stylesheet" href="css/custom.css">
            <link rel="stylesheet" href="css/font-awesome.min.css">

        </head>

        <body>
        <!-- Navbar top -->
        <?php include('includes/student_nav.php'); ?>
        <!-- Navbar end here-->



        <!--Page Body-->

        <div class="row_margin_h">
            <h2 class="header_text"> Welcome to Exam Seat Management System</h2>
        </div>
        <div class="col-md-2">

        </div>

        <div class="col-md-8 column_color row_margin_bottom">
            <h3 class="header_text">Seatplan </h3>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Course Code</th>
                        <th>Section</th>
                        <th>Room No</th>
                        <th>Column No</th>
                        <th>Total Seat</th>
                        <th>Semester</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $sql="SELECT * FROM course where student_id='$student_id' and semester='$semester'";
                        $run=mysqli_query($connection,$sql);
                        while ($result=mysqli_fetch_array($run))
                        {
                            $course_code=$result['course_code'];
                            $section=$result['section'];

                            $sql2="SELECT * FROM room_details where course_code='$course_code' and section='$section' and semester='$semester'";
                            $run2=mysqli_query($connection,$sql2);
                            while ($res=mysqli_fetch_array($run2))
                            {
                                $r_id=$res['r_id'];
                                $room_no=$res['room_no'];
                                $time=$res['time'];
                                $course_codef=$res['course_code'];
                                $sectionf=$res['section'];
                                $column_no=$res['column_no'];
                                $total_seat=$res['total_seat'];
                                $semesterf=$res['semester'];
                                $date=$res['date'];
                                $teacher1=$res['teacher1'];
                                $teacher2=$res['teacher2'];

                    ?>
                    <tr>
                        <td><?php echo $r_id?></td>
                        <td><?php echo $date?></td>
                        <td><?php echo $time?></td>
                        <td><?php echo $course_codef?></td>
                        <td><?php echo $sectionf?></td>
                        <td><?php echo $room_no?></td>
                        <td><?php echo $column_no?></td>
                        <td><?php echo $total_seat?></td>
                        <td><?php echo $semesterf?></td>
                    </tr>
                    <?php }}?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-2">

        </div>
        <!-- Body End-->


        <!--NAavbar bottom-->

        <?php include('includes/footer.php'); ?>

        <!--JavaScript here-->
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

        <script  src="js/index.js"></script>

        </body>
        </html>
    <?php } }?>