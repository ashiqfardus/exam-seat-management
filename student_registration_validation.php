<?php
/**
 * Created by PhpStorm.
 * User: Ashiq Fardus
 * Date: 3/11/2018
 * Time: 11:07 PM
 */
include 'includes/connection.php';

function sreg()
{
    global $connection;

    $result=array(
      'result'=>1,
      'message'=>'undefined error'
    );

    if (isset($_POST['reg']))
    {
        $name=$_POST['name'];
        $email=$_POST['email'];
        $id_no=$_POST['id_no'];
        $password=$_POST['password'];
        $password2=$_POST['password2'];

        //Email check if exists or not
        $emailcheck=mysqli_query($connection,"Select email from student_information WHERE email='$email'");
        $echeck=mysqli_num_rows($emailcheck);
        if ($echeck!=0)
        {
            $result['message']='Email already exists';
            return $result;
        }

        //Id no check if exists or not
        $idchk=mysqli_query($connection,"Select email from student_information WHERE student_id='$id_no'");
        $id_check=mysqli_num_rows($idchk);
        if ($id_check!=0)
        {
            $result['message']='ID already exists';
            return $result;
        }

        //Checking empty field
        if (empty($name)|| empty($email)|| empty($password)||empty($password2) || empty($id_no))
        {
            $result['message']='Any of the field is empty';
            return $result;
        }

        //Password length check
        if (strlen($password)>20 || strlen($password)<6)
        {
            $result['message']='Password length must be between 6-20 character';
            return $result;
        }

        //Password matching
        if ($password != $password2)
        {
            $result['message']="Password didn't match";
            return $result;
        }

        $password=md5($password);
        $query="INSERT INTO student_information(name,email,password,student_id) VALUES ('$name','$email','$password','$id_no')";
        if (mysqli_query($connection,$query))
        {
            $result['result']=0;
            $result['message']='Registration completed. Please Login Now.';
            return $result;
        }
    }
}