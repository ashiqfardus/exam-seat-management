<?php
include 'includes/connection.php';
if (!isset($_SESSION))
{
    session_start();
}
if(!isset($_SESSION['email']))
{
    header("location: u_log.php");
}
else
{
$email=$_SESSION['email'];
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Exam Seat Handling</title>
    <link rel='stylesheet prefetch' href='css/bootstrap.css'>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
  <style type="text/css">
  	.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload{
    width: 20%;
    margin-top: 10px;
}
  </style>
</head>

<body>
	<!-- Navbar top -->
		<?php include('includes/teacher_nav.php'); ?>
	<!-- Navbar end here-->

    <!--teacher data read from database-->
    <?php
        $query="SELECT * FROM teacher WHERE email= '$email'";
        $run=mysqli_query($connection,$query);
        while ($data=mysqli_fetch_array($run))
        {
            $id=$data['t_id'];
            //$email=$data['email'];
            $name=$data['name'];
            $dob=$data['dob'];
            $phone=$data['phone'];
            $initial=$data['initial'];
            $image=$data['image'];

    ?>

	<!--Page Body-->
            <div class="col-md-3">

            </div>
            <div class="col-md-6">
                <div class="row_margin_bottom row_margin_h">
                    <h2 class="header_text2"><b>Edit Your Profile</b></h2>

                    <form action="edit_teacher_profile.php?update_id=<?php echo $id ?>" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-xs-12 ">
                                    <label>Upload Image</label>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default btn-file">
                                                Browse… <input type="file" id="imgInp" name="image">
                                            </span>
                                        </span>
                                        <input type="text" class="form-control" readonly>
                                    </div>
                                    <img id='img-upload'  />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <label for="usr">Name:</label>
                                    <input type="text" class="form-control" id="usr" name="name" value="<?php if (isset($name)){ echo $name; } ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <label for="usr">Phone:</label>
                                    <input type="text" class="form-control" id="usr" name="phone" value="<?php if (isset($phone)){ echo $phone; } ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <label for="usr">Gender:</label>
                                    <select class="form-control" id="sel1" name="gender">
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <label for="usr">Date Of Birth:</label>
                                    <input type="Date" class="form-control" id="usr" name="dob" value="<?php if (isset($dob)){ echo $dob; } ?>" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-offset-0 col-sm-12 row_margin text_right">
                                    <button type="submit" name="submit" class="btn btn-default">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-3">

            </div>
    <?php } ?>
    <!-- Body End-->

    <?php
    require_once 'teacher_edit_validation.php';
    $result=tedit();

    //        Showing messages

    if ($result)
    {
        if ($result['result']==0)
        {
            ?>
            <div class="col-md-4">
            </div>

            <div class="col-md-4 row_margin">
                <div class="alert alert-success" role="alert">
                    <?php echo $result['message']; ?>
                </div>
            </div>
                <script>
                    window.location.href='profile_teacher.php?id=<?php echo $id?>';
                </script>
            <?php
        }
        else
        {
        ?>

                    <div class="col-md-4">
                    </div>

                    <div class="col-md-4 row_margin">
                        <div class="alert alert-danger" role="alert">
                            <strong>Error!</strong> <?php echo $result['message']; ?>
                        </div>
                    </div>

            <?php
        }
    }
    ?>
    <!--NAavbar bottom-->
    
	<?php include('includes/footer.php'); ?>
		
		<!--JavaScript here-->
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

    <script  src="js/index.js"></script>

</body>
</html>
<script type="text/javascript">
        $(document).ready( function() {
            $(document).on('change', '.btn-file :file', function() {
                var input = $(this),
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [label]);
            });

            $('.btn-file :file').on('fileselect', function(event, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = label;

                if( input.length ) {
                    input.val(log);
                } else {
                    if( log ) alert(log);
                }

            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#img-upload').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#imgInp").change(function(){
                readURL(this);
            });
        });
    </script>

<?php } ?>