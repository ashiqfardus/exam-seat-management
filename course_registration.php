<?php require 'includes/connection.php';
if (!isset($_SESSION))
{
    session_start();
}
if(!isset($_SESSION['email']))
{
    header("location: u_log.php");
}
else
{
$email=$_SESSION['email'];
$query="SELECT * FROM student_information WHERE email= '$email' OR student_id='$email'";
$run=mysqli_query($connection,$query);
while ($data=mysqli_fetch_array($run))
{
$id=$data['id'];
$student_id=$data['student_id'];
$name=$data['name'];
$phone=$data['phone'];
$dob=$data['dob'];
$gender=$data['gender'];
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Exam Seat Handling</title>
    <link rel='stylesheet prefetch' href='css/bootstrap.css'>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
  
</head>

<body>
	<!-- Navbar top -->
		<?php include('includes/student_nav.php'); ?>
	<!-- Navbar end here-->

    <!-- Page body-->
    <div class="container row_margin_h row_margin_bottom">
        <h1 class="button_alignment" style="margin-bottom: 20px;"> Course registration. </h2>
            <div class="col-md-3">
            </div>
            <div class="col-md-6 column_color">
                <form action="course_registration.php?id=<?php echo $student_id ?>" method="post">
                    <div class="form-group">
                        <label for="sel1">Select semester:</label>
                        <select class="form-control" name="semester">
                            <option value="">Select Semester</option>
                            <?php
                                $sql="SELECT * FROM semester ORDER BY semester_no";
                                $run=mysqli_query($connection,$sql);
                                while ($result=mysqli_fetch_array($run)) {
                                    $semester_id = $result['semester_id'];
                                    $semester_no = $result['semester_no'];
                                    ?>
                                    <option value="<?php echo $semester_no ?>"><?php echo $semester_no?></option>
                                    <?php
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-6">
                            <label for="ex3">Course Code</label>
                            <input class="form-control" type="text" name="course" placeholder="EX: CSE 421">
                        </div>
                        <div class="col-xs-6">
                            <label for="ex3">Section</label>
                            <input class="form-control" id="ex3" type="text" name="section" placeholder="EX: A">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-default" name="submit">Submit</button>
                </form>
            </div>
            <div class="col-md-3">
            </div>
    </div>

    <?php
        require_once 'course_registration_validation.php';
        $result=course_reg();

        if ($result)
        {
            if ($result['result']==0)
            {
                ?>
                <div class="col-md-4">

                </div>
                <div class="col-md-4">
                    <div class="alert alert-success" role="alert">
                        <strong>Success!</strong> <?php echo $result['message']; ?>
                    </div>
                </div>
                <?php
            }
            else
            {
                ?>
                <div class="col-md-4">

                </div>
                <div class="col-md-4">
                    <div class="alert alert-danger" role="alert">
                        <strong>Error!</strong> <?php echo $result['message']; ?>
                    </div>
                </div>
                <?php
            }
        }
    ?>
    <!--page body end-->

	<?php include('includes/footer.php'); ?>
		
		<!--JavaScript here-->
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

    <script  src="js/index.js"></script>

</body>
</html>
<?php }} ?>