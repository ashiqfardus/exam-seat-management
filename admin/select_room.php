<?php include '../includes/connection.php';
session_start();
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Exam Seat Handling</title>
    <link rel='stylesheet prefetch' href='../css/bootstrap.css'>
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/custom.css">
</head>

<body>
<!-- Navbar top -->

<div class="row">
    <nav class="navbar navbar-default nav_color navbar-fixed-top">
        <h3 style="color: white; text-align: center; font-weight: bold;">Exam Seat Management</h3>
    </nav>
</div>
<!--Page Body-->

<div class="container ">
    <div class="col-md-12 col-sm-12">
        <div class="container-fluid">
            <div class="row row_margin_h">
                <h1 class="header_text"> Welcome to The Exam Seat Management System </h1>
            </div>
            <div class="row ">
                <form class="form form-vertical" action="select_room.php" method="post">
                    <div class="row">
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-6">
                            <div class="row form-group">
                                <label for="ex3">Semester</label>
                                <select class="form-control" id="sel1" name="semester">
                                    <option value="">Select Semester</option>
                                    <?php
                                    $query="SELECT DISTINCT semester from room_details";
                                    $run=mysqli_query($connection,$query);
                                    while ($result=mysqli_fetch_array($run))
                                    {
                                        $semester=$result['semester'];
                                        ?>
                                        <option value="<?php echo $semester?>"><?php echo $semester?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="row form-group">
                                    <label for="ex3">Room No</label>
                                    <select class="form-control" id="sel1" name="room_no">
                                        <option value="">Select Room</option>
                                        <?php
                                            $query="SELECT DISTINCT room_no from room_details";
                                            $run=mysqli_query($connection,$query);
                                            while ($result=mysqli_fetch_array($run))
                                            {
                                                $room_id=$result['r_id'];
                                                $room_no=$result['room_no'];
                                        ?>
                                            <option value="<?php echo $room_no?>"><?php echo $room_no?></option>
                                        <?php } ?>
                                    </select>
                            </div>
                            <div class="row form-group">
                                <label for="ex3">Time</label>
                                <select class="form-control" id="sel1" name="time">
                                    <option value="">Select Time</option>
                                    <?php
                                    $query="SELECT DISTINCT time from room_details";
                                    $run=mysqli_query($connection,$query);
                                    while ($result=mysqli_fetch_array($run))
                                    {
                                        $time=$result['time'];
                                        ?>
                                        <option value="<?php echo $time?>"> <?php echo $time?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="row form-group">
                                <label for="ex3">Date</label>
                                <select class="form-control" id="sel1" name="date">
                                    <option value="">Select Date</option>
                                    <?php
                                    $query="SELECT DISTINCT date from room_details";
                                    $run=mysqli_query($connection,$query);
                                    while ($result=mysqli_fetch_array($run))
                                    {
                                        $date=$result['date'];
                                        ?>
                                        <option value="<?php echo $date?>"> <?php echo $date?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <div class="text-center">
                                    <button type="submit" name="submit" class="btn btn-default">Submit</button>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-3">
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<!-- Body End-->
<?php
if (isset($_POST['submit']))
{
    $semester=$_POST['semester'];
    $room_no=$_POST['room_no'];
    $time=$_POST['time'];
    $date=$_POST['date'];

    if (empty($room_no)||empty($time)||empty($date)||empty($semester))
    {
        ?>
        <div class="col-md-3">

        </div>
        <div class="col-md-6">
            <div class="alert alert-danger row_margin_bottom" role="alert">
                <strong>Error!</strong> Any of the field is empty!
            </div>
        </div>
        <?php
    }
    else
    {
        $_SESSION['semester']=$semester;
        $_SESSION['room_no']=$room_no;
        $_SESSION['time']=$time;
        $_SESSION['date']=$date;
        ?>
        <script>
            window.location.href='../room_details.php';
        </script>
        <?php
    }
}
?>

<!--NAavbar bottom-->
<?php include('../includes/footer.php'); ?>
<!--JavaScript here-->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

<script  src="../js/index.js"></script>

</body>
</html>