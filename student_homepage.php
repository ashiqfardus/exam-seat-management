<?php
require_once "includes/connection.php";
if (!isset($_SESSION))
{
    session_start();
}
if(!isset($_SESSION['email']))
{
    header("location: u_log.php");
}
else
{
$email=$_SESSION['email'];
$sql="SELECT * FROM student_information where email='$email' or student_id='$email'";
$run=mysqli_query($connection,$sql);
while ($data=mysqli_fetch_array($run)) {
    $id = $data['id'];
    $student_id = $data['student_id'];
    $name = $data['name'];
    $phone = $data['phone'];
    $dob = $data['dob'];
    $gender = $data['gender'];
    $semester = $data['semester'];
    $rfid = $data['rfid_tag'];
}
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Exam Seat Handling</title>
    <link rel='stylesheet prefetch' href='css/bootstrap.css'>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/custom.css">
    <script src="https://use.fontawesome.com/6e655bd209.js"></script>
  
</head>

<body>
	<!-- Navbar top -->
		<?php include('includes/student_nav.php'); ?>
	<!-- Navbar end here-->



	<!--Page Body-->
	    <div class="container">
	    	<!-- Header Row-->
	    	<div class="row row_margin_home">
	    		<h2 class="header_text"> Welcome to Exam Seat Management System</h2>
	    	</div>

	    	<!-- Details Row-->
	       <div class="row row_margin">
	       		<!-- Details part-->
	       		<div class="col-md-5 column_color">
	       			<div class="row">
	       				<h3 class="header_text">Details</h3>
	       			</div>
	       			<div class="row row_margin">
	       				<p class="p_text">Name: <?php echo $name?>  </p>
	       				<p class="p_text">Id: <?php echo $student_id?> </p>
	       				<p class="p_text">Semester: <?php if (empty($semester)) {echo "Add Semester";} else {echo $semester;}?> </p>
	       				<p class="p_text">Date Of Birth: <?php echo $dob?></p>
	       				<p class="p_text">RFID Status: <?php if (empty($rfid)){echo "Not Added";} else{echo "Added";}?></p>
	       			</div>
	       		</div>
	       		<div class="col-md-1">
	       		</div>


	       		<!-- Course and section part-->
	       		<div class="col-md-5 column_color">
	       			<h3 class="header_text">Course & Section</h3>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Course Code</th>
                                <th>Section</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($semester))
                            {
                            $query=mysqli_query($connection,"SELECT * FROM course where student_id='$student_id' and semester='$semester'");
                            while($row=mysqli_fetch_array($query))
                            {
                                $id=$row['c_id'];
                                $course_code=$row['course_code'];
                                $section=$row['section'];
                                ?>

                                <tr>
                                    <td><?php if (!empty($id)){echo $id;}?></td>
                                    <td><?php if (!empty($course_code)){echo $course_code;}?></td>
                                    <td><?php if (!empty($section)){echo $section;}?></td>
                                </tr>
                            <?php }} ?>
                            </tbody>
                        </table>
                    </div>
	       		</div>


	       </div>
	       <!-- Details row end-->

	       <div class="row row_margin row_margin_bottom">
	       		<div class="col-md-12 column_color">
	       			<h3 class="button_alignment">Instructions </h3>
	       			<p class="p_text"> 1. At first edit your profile and course registration. Otherwise you will not be able attend in the exam. </p>
	       			<p class="p_text"> 2. If you enter any worng data then authority will not be responsible for not getting seat in the exam hall. </p>
	       			<p class="p_text"> 3. Provide your personal information correctly </p>
	       			<p class="p_text"> 4. Don't enter other's id card while scanning id. </p>
	       		</div>
	       </div>


	    </div>
    <!-- Body End-->


    <!--NAavbar bottom-->
    
    <?php include('includes/footer.php'); ?>
		
		<!--JavaScript here-->
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

    <script  src="js/index.js"></script>

</body>
</html>
<?php } ?>