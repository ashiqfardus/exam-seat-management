<?php
/**
 * Created by PhpStorm.
 * User: Ashiq Fardus
 * Date: 2/21/2018
 * Time: 3:41 PM
 */
require '../includes/connection.php';

function add_room()
{
    global $connection;
//    Using result for error messages
    $result=array(
        'result'=>'1',
        'message'=>'Undefined Error'
    );
//    Form Validation
    if (isset($_POST['submit']))
    {
        $room_no=$_POST['room_no'];
        $capacity=$_POST['capacity'];

//        Email check if exists or not
        $room_check=mysqli_query($connection,"Select room_no from room WHERE room_no='$room_no'");
        $rcheck=mysqli_num_rows($room_check);
        if ($rcheck!=0)
        {
            $result['message']='Room already exists';
            return $result;
        }


//        Checking empty field
        if (empty($room_no))
        {
            $result['message']='The field is empty';
            return $result;
        }

        //Capacity Check
        if ($capacity>45)
        {
            $result['message']='Maximum capacity is 45';
            return $result;
        }

        $query="INSERT INTO room(room_no,capacity) VALUES ('$room_no','$capacity')";
        if (mysqli_query($connection,$query))
        {
            $result['result']=0;
            $result['message']='Room has been added';
            return $result;
        }
    }
}

function update_room()
{
    global $connection;
//    Using result for error messages
    $result=array(
        'result'=>'1',
        'message'=>'Undefined Error'
    );
//    Form Validation
    if (isset($_POST['submit']))
    {
        $id=$_GET['id'];
        //$room_no=$_POST['room_no'];
        $capacity=$_POST['capacity'];

//        Email check if exists or not
        //$room_check=mysqli_query($connection,"Select room_no from room WHERE room_no='$room_no'");
        //$rcheck=mysqli_num_rows($room_check);
//        if ($rcheck>1)
//        {
//            $result['message']='Room already exists';
//            return $result;
//        }


//        Checking empty field
//        if (empty($room_no))
//        {
//            $result['message']='The field is empty';
//            return $result;
//        }

        //Capacity Check
        if ($capacity>45)
        {
            $result['message']='Maximum capacity is 45';
            return $result;
        }

        $query="UPDATE room SET capacity='$capacity' where id='$id'";
        if (mysqli_query($connection,$query))
        {
            $result['result']=0;
            $result['message']='Room has been updated';
            return $result;
        }
    }
}