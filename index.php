<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Exam Seat Handling</title>
    <link rel='stylesheet prefetch' href='css/bootstrap.css'>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/custom.css">
</head>

<body>
<!-- Navbar top -->

<div class="row">
    <nav class="navbar navbar-default nav_color navbar-fixed-top">
        <h3 style="color: white; text-align: center; font-weight: bold;">Exam Seat Management</h3>
    </nav>
</div>
<!--Page Body-->

<div class="container row_margin_h">
    <div class="col-md-12 col-sm-12">
        <div class="container-fluid">
            <div class="row row_margin_h">
                <h1 class="header_text"> Welcome to The Exam Seat Management System </h1>
            </div>
            <div class="row row_margin">
                <div class="button_alignment">
                    <a href="admin/select_room.php" class="btn button_info btn-lg" role="button">Seat </a>
                </div>
            </div>
            <div class="row row_margin">
                <div class="button_alignment">
                    <a href="u_log.php" class="btn button_info btn-lg" role="button">Login</a>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Body End-->


<!--NAavbar bottom-->
<?php include('includes/footer.php'); ?>
<!--JavaScript here-->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

<script  src="js/index.js"></script>

</body>
</html>
