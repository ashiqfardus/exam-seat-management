<?php
/**
 * Created by PhpStorm.
 * User: Ashiq Fardus
 * Date: 5/30/2018
 * Time: 12:46 AM
 */
require '../includes/connection.php';

function add_time()
{
    global $connection;
//    Using result for error messages
    $result=array(
        'result'=>'1',
        'message'=>'Undefined Error'
    );
//    Form Validation
    if (isset($_POST['submit']))
    {
        $time_list=strtoupper($_POST['time_table']);

//        Email check if exists or not
        $room_check=mysqli_query($connection,"Select time_list from time WHERE time_list='$time_list'");
        $rcheck=mysqli_num_rows($room_check);
        if ($rcheck!=0)
        {
            $result['message']='Time already set.';
            return $result;
        }


//        Checking empty field
        if (empty($time_list))
        {
            $result['message']='The field is empty';
            return $result;
        }


        $query="INSERT INTO time(time_list) VALUES ('$time_list')";
        if (mysqli_query($connection,$query))
        {
            $result['result']=0;
            $result['message']='Time has been added';
            return $result;
        }
    }
}

function update_time()
{
    global $connection;
//    Using result for error messages
    $result=array(
        'result'=>'1',
        'message'=>'Undefined Error'
    );
//    Form Validation
    if (isset($_POST['submit']))
    {
        $id=$_GET['id'];
        $time_list=strtoupper($_POST['time_table']);

//        Email check if exists or not
        $room_check=mysqli_query($connection,"Select time_list from time WHERE time_list='$time_list'");
        $rcheck=mysqli_num_rows($room_check);
        if ($rcheck!=0)
        {
            $result['message']='Time already set.';
            return $result;
        }


//        Checking empty field
        if (empty($time_list))
        {
            $result['message']='The field is empty';
            return $result;
        }


        $query="UPDATE time SET time_list='$time_list' where id='$id'";
        if (mysqli_query($connection,$query))
        {
            $result['result']=0;
            $result['message']='Time has been updated';
            return $result;
        }
    }
}