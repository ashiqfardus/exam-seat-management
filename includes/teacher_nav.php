<?php require 'connection.php';
if (!isset($_SESSION))
{
    session_start();
}
if(!isset($_SESSION['email']))
{
    header("location: u_log.php");
}
else
{
    $email=$_SESSION['email'];
    $query="SELECT * FROM teacher WHERE email= '$email'";
    $run=mysqli_query($connection,$query);
    while ($data=mysqli_fetch_array($run))
    {
        $id=$data['t_id'];
        //$email=$data['email'];
        $name=$data['name'];
        $dob=$data['dob'];
        $phone=$data['phone'];
        $initial=$data['initial'];
        $image=$data['image'];
?>
<div class="container-fluid">
		<nav class="navbar navbar-default nav_color navbar-fixed-top">
		  <div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="teacher_homepage.php">ESM</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <ul class="nav navbar-nav">
				<li><a href="profile_teacher.php?id=<?php echo $id ?>">Profile <span class="sr-only">(current)</span></a></li>
				<li><a href="teacher_exam_duty.php?id=<?php echo $id?>">Exam Invigilation </a></li>
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li><a href="profile_teacher.php?id=<?php echo $id ?>"><?php echo $name?></a></li>
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu <span class="caret"></span></a>
				  <ul class="dropdown-menu">
				  	<li><a href="profile_teacher.php?id=<?php echo $id ?>"><i class="fa fa-user" aria-hidden="true"></i> profile</a></li>
					<li><a href="edit_teacher_profile.php?id=<?php echo $id ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Profile</a></li>
					<li><a href="teacher_exam_duty.php?id=<?php echo $id?>"><i class="fa fa-list-ol" aria-hidden="true"></i> Exam Duty</a></li>
					<li><a href="teacher_pass_change.php?id=<?php echo $id ?>"><i class="fa fa-unlock-alt" aria-hidden="true"></i> Change Password</a></li>
					<li role="separator" class="divider"></li>
					
					<li><a href="logout.php"><i class="fa fa-user" aria-hidden="true"></i> Logout</a></li>
				  </ul>
				</li>
			  </ul>
			</div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</div>
	<script src="https://use.fontawesome.com/6e655bd209.js"></script>
<?php } }?>