<?php
/**
 * Created by PhpStorm.
 * User: Ashiq Fardus
 * Date: 8/12/2018
 * Time: 2:08 AM
 */
require 'includes/connection.php';
if (!isset($_SESSION))
{
    session_start();
}
$semester=$_SESSION['semester'];
$room_no=$_SESSION['room_no'];
$time=$_SESSION['time'];
$date=$_SESSION['date'];
?>
<?php
//$page = $_SERVER['PHP_SELF'];
//$sec = "5";
//?>
<?php
$url1=$_SERVER['REQUEST_URI'];
header("Refresh: 5; URL=$url1");
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
<!--    <meta http-equiv="refresh" content="--><?php //echo $sec?><!--;URL='--><?php //echo $page?><!--'">-->
    <title>Exam Seat Handling</title>
    <link rel='stylesheet prefetch' href='css/bootstrap.css'>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/custom.css">
</head>

<body>
<!-- Navbar top -->

<div class="row">
    <nav class="navbar navbar-default nav_color navbar-fixed-top">
        <h3 style="color: white; text-align: center; font-weight: bold;">Exam Seat Management</h3>
    </nav>
</div>
<!--Page Body-->

<div class="container row_margin_h">
    <div class="col-md-12 col-sm-12">
        <div class="row">
            <h3 class="button_alignment" style="font-weight: bold;"><?php echo $semester?></h3>
        </div>
        <div class="row">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Room No</th>
                    <th>Time</th>
                    <th>Exam invigilator</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $q=mysqli_query($connection,"SELECT * FROM room_details where room_no='$room_no' and time='$time' and date='$date' and semester='$semester'");
                while ($res=mysqli_fetch_array($q))
                {
                $room_no=$res['room_no'];
                $time=$res['time'];
                $teacher1=$res['teacher1'];
                $teacher2=$res['teacher2'];
                ?>
                <tr>
                    <td><?php echo $room_no?></td>
                    <td><?php echo $time?></td>
                    <td><?php echo $teacher1; if (!empty($teacher2)){ echo ' & '.$teacher2;}?> </td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="row row_margin">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Course Code</th>
                    <th>Section</th>
                    <th>Column</th>
                    <th>Total Seat</th>
                    <th>Remaining Seat</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $q=mysqli_query($connection,"SELECT * FROM room_details where room_no='$room_no' and time='$time' and date='$date'");
                while ($res=mysqli_fetch_array($q))
                {

                $course_code=$res['course_code'];
                $section=$res['section'];
                $column=$res['column_no'];
                $total_seat=$res['total_seat'];
                $remaining_seat=$res['remaining_seat'];
                ?>
                <tr>
                    <td><?php echo $course_code?></td>
                    <td><?php echo $section?></td>
                    <td><?php echo $column?></td>
                    <td><?php echo $total_seat?></td>
                    <td><?php echo $remaining_seat?></td>
                </tr>

                <?php } ?>

                </tbody>
            </table>
        </div>
        <div class="row row_margin">
            <h3 class="button_alignment" style="font-weight: bold;">Scan Your RFID to Enter into The Room</h3>
             <form class="form-group form" action="room_details.php" method="get">
                 <div class="form-group row">
                     <div class="col-xs-3">
                     </div>
                     <div class="col-xs-5">
                         <input class="form-control button_alignment" id="ex3" type="text" placeholder="143-15-4639" name="rfid">
                     </div>
                     <div class="col-xs-3">
                         <button type="submit" class="btn btn-default">Submit</button>
                     </div>
                 </div>
             </form>
        </div>




    </div>
</div>

<!-- Body End-->


<!--NAavbar bottom-->
<?php include('includes/footer.php');

//if (mysqli_num_rows($course_search)>0)
//{
//    while ($course_result=mysqli_fetch_array($course_search))
//    {
//        $course_codes=$course_result['course_code'];
//        $sections=$course_result['section'];
//
//        if ($course_code==$course_codes)
//        {
//            if ($section==$sections)
//            {
//                echo "<br>";
//                echo "Course matching";
//                echo "<br>";
//                echo $course_code. 'matched'. $course_codes;
//                echo "<br>";
//                echo "<br>";
//                echo "Section matching";
//                echo "<br>";
//                echo $section . 'matched' .  $sections;
//                echo "<br>";
//            }
//            else
//            {
//                echo "section didn't match";
//            }
//
//        }
//        else
//        {
//            echo "Course didn't match";
//        }
//    }
//}
//else
//{
//    echo "You don't have nothing";
//}

?>
<?php
if (isset($_GET['rfid']))
{
    $rfid=$_GET['rfid'];
    $q2=mysqli_query($connection,"SELECT * FROM student_information where rfid_tag='$rfid'");
    if ($result=mysqli_fetch_array($q2))
    {
        $student_id=$result['student_id'];
        $q=mysqli_query($connection,"SELECT * FROM room_details where room_no='$room_no' and time='$time' and date='$date'");
        while ($res=mysqli_fetch_array($q)) {

            $course_code= $res['course_code'];
            $section = $res['section'];

            $course_search=mysqli_query($connection,"SELECT * FROM course where student_id='$student_id'
                                                                            and semester='$semester' and course_code='$course_code'
                                                                            and course_code='$course_code'");

            if (mysqli_num_rows($course_search)>0)
            {
                while ($course_result=mysqli_fetch_array($course_search))
                {
                    $courses=$course_result['course_code'];
                    $sections=$course_result['section'];

                    if ($section==$sections)
                    {
                        $select_query=mysqli_query($connection,"SELECT * FROM exam_hall_details where student_id='$student_id'
                                                        and course_code='$courses' and section='$sections' and
                                                        semester_no='$semester' and time='$time' and date='$date'");
                        if (mysqli_num_rows($select_query)>0)
                        {

                            ?>
                            <div class="col-md-4">

                            </div>
                            <div class="col-md-4">
                                <div class="alert alert-danger" role="alert">
                                    <strong>Error!</strong> You have already entered into a room
                                </div>
                            </div>
                            <?php
                        }

                        else
                        {
                            $sql="INSERT INTO exam_hall_details(room_no,student_id,course_code,section,semester_no,time,date)
                                                values ('$room_no','$student_id','$courses','$sections','$semester','$time','$date')";

                            if (mysqli_query($connection,$sql))
                            {
                                $update="UPDATE room_details set remaining_seat=$remaining_seat-1 where
                                                    room_no='$room_no' and time ='$time' and course_code='$courses' and section='$sections' and date='$date'";
                                if (mysqli_query($connection,$update))
                                {
                                    ?>
                                    <div class="col-md-4">

                                    </div>
                                    <div class="col-md-4">
                                        <div class="alert alert-success" role="alert">
                                            <strong>Success!</strong> You have entered into this room
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        }
                    }
                    else
                    {
                        ?>
                        <div class="col-md-4">

                        </div>
                        <div class="col-md-4">
                            <div class="alert alert-danger" role="alert">
                                <strong>Error!</strong> You are not eligible for this room.
                            </div>
                        </div>
                        <?php
                    }
                }
            }
            else
            {
                ?>
                <div class="col-md-4">

                </div>
                <div class="col-md-4">
                    <div class="alert alert-danger" role="alert">
                        <strong>Error!</strong> You are not eligible for this room.
                    </div>
                </div>
                <?php
            }
        }
    }


    echo "
    
    ";
}
?>
<!--JavaScript here-->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

<script  src="js/index.js"></script>

</body>
</html>
