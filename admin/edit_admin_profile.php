<?php require '../includes/connection.php';
if (!isset($_SESSION))
{
    session_start();
}
if(!isset($_SESSION['email']))
{
    header("location: admin_login.php");
}
else
{
$email=$_SESSION['email'];
$query="SELECT * FROM admin WHERE email= '$email' OR username='$email'";
$run=mysqli_query($connection,$query);
while ($data=mysqli_fetch_array($run))
{
$id=$data['a_id'];
$username=$data['username'];
$email=$data['email'];
?>
<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Exam Seat Handling</title>
    <link rel='stylesheet prefetch' href='../css/bootstrap.css'>
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/custom.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
  
</head>

<body>
	<!-- Navbar top -->
		<?php include('../includes/admin_home_nav.php'); ?>
	<!-- Navbar end here-->



	<!--Page Body-->
	    <div class="container row_margin_h">
	    	<h1 class="button_alignment" style="margin-bottom: 20px;"> Edit Your Personal Information </h1>
	    	<div class="row row_margin_bottom">
	    		<form class="form form-vertical" action="edit_admin_profile.php?id=<?php echo $id ?>" method="post" enctype="multipart/form-data">
				    <div class="row">
				    	<div class="col-sm-2">
				    	</div>
				        <div class="col-sm-8">
				          <div class="row">
				            <div class="col-sm-6">
				              <div class="form-group">
				                <label for="name">Username<span class="kv-reqd"></span></label>
				                <input type="text" class="form-control" name="username" value="<?php echo $username?>">
				              </div>
				            </div>
				            <div class="col-sm-6">
				              <div class="form-group">
				                <label for="initial">Email<span class="kv-reqd"></span></label>
				                <input type="text" class="form-control" name="email" value="<?php echo $email?>">
				              </div>
				            </div>
				          </div>
				          <div class="form-group">
				            <div class="text-right"> 
				              <button type="submit" name="submit" class="btn btn-default">Submit</button>
				            </div>
				          </div>
				        </div>
				        <div class="col-sm-2">
				    	</div>
				    </div>
				</form>
	    	</div>
	    </div>
    <!-- Body End-->

    <!--Edit action -->
    <?php
    require_once 'edit_admin_profile_validation.php';
    $result=aedit();

    //        Showing messages

    if ($result)
    {
        if ($result['result']==0)
        {
            ?>
            <div class="col-md-4">
            </div>

            <div class="col-md-4 row_margin">
                <div class="alert alert-success" role="alert">
                    <?php echo $result['message']; ?>
                </div>
            </div>
            <script>
                window.location='profile_admin.php?id=<?php echo $id?>';
            </script>
        <?php
        }
        else
        {
        ?>

            <div class="col-md-4">
            </div>

            <div class="col-md-4 row_margin">
                <div class="alert alert-danger" role="alert">
                    <strong>Error!</strong> <?php echo $result['message']; ?>
                </div>
            </div>

            <?php
        }
    }
    ?>


    <!--NAavbar bottom-->
    
	<?php include('../includes/footer.php'); ?>
		
		<!--JavaScript here-->
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>

    <script  src="../js/index.js"></script>

</body>
</html>
<?php }} ?>